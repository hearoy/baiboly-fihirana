import { from, Observable, asyncScheduler as async, of, defer, scheduled } from 'rxjs';
import { BaibolyJson, BokyJson, BokyRefs } from '../model/baiboly.model';
import { MetricsSvc } from '../tools/metrics';

export class BaibolyPuller {
   bibleFr: BokyJson[];
   private baiboly: BaibolyJson;
   private bokyRefs: BokyRefs[];
   private requestBaiboly;
   requestBibleFr: Request;
   private requestBokyRefs;
   static instance: BaibolyPuller | null = null;
   private metricsSvc: MetricsSvc;

   constructor() {
      if (BaibolyPuller.instance != null)
         return BaibolyPuller.instance;
      else {
         const headers = new Headers();
         headers.set('Accept', 'application/json');
            this.requestBaiboly = new Request('/data/baiboly.json', {
               method: 'get',
               headers
            });
            this.requestBibleFr = new Request('/data/bibleFr.json', {
               method: 'get',
               headers
               // cache: 'force-cache'
            });
            this.requestBokyRefs = new Request('/data/bokyRefs.json', {
               method: 'get',
               headers
               // cache: 'force-cache'
            });
         BaibolyPuller.instance = this;
         this.metricsSvc = new MetricsSvc();
      }
   }

   private _async(func: Function) {
      return new Promise(res => {
         setTimeout(() => {
            res(func());
         }, 0);
      })
   }
   private _fetchBaiboly$(): Observable<BaibolyJson> {
      if (this.baiboly)
         return scheduled(Promise.resolve(1).then<BaibolyJson>(res => this.baiboly), async);
      else {
         return defer(() => {
            if (this.baiboly) {
               return Promise.resolve(this.baiboly);
            }
            this.metricsSvc.timeStart('fetchAndParseBaiboly');
            return fetch(this.requestBaiboly)
               .then(res => res.json())
               .then(obj => {
                  this.metricsSvc.sendTiming({
                     name: 'baiboly',
                     value: this.metricsSvc.timeEnd('fetchAndParseBaiboly'),
                     event_category: 'JS perfs',
                     event_label: 'fetch_and_parse'
                  });
                  this.baiboly = obj;
                  return this.baiboly;
               });
         });
      }
   }
   private _fetchBible$(): Observable<BaibolyJson> {
      if (this.bibleFr)
         return scheduled(Promise.resolve(1).then<BaibolyJson>(res => this.bibleFr), async);
      else {
         return defer(() => {
            if (this.bibleFr) {
               return Promise.resolve(this.bibleFr);
            }
            this.metricsSvc.timeStart('fetchAndParseBibleFr');
            return fetch(this.requestBibleFr)
               .then(res => res.json())
               .then((obj: BaibolyJson) => {
                  this.metricsSvc.sendTiming({
                     name: 'bibleFr',
                     value: this.metricsSvc.timeEnd('fetchAndParseBibleFr'),
                     event_category: 'JS perfs',
                     event_label: 'fetch_and_parse'
                  });
                  this.bibleFr = obj;
                  return this.bibleFr;
               });
         });
      }
   }

   private _fetchBokyRef$(): Observable<BokyRefs[]> {
      {//if(what.match('bokyRefs')){
         // let promiseBokyRefs: () => Promise<BokyRefs[]>;
         if (this.bokyRefs)
            return of(this.bokyRefs);
         else {
            return defer(() => {
               if (this.bokyRefs) {
                  return Promise.resolve(this.bokyRefs);
               }
               this.metricsSvc.timeStart('fetchAndParsebokyRefs');
               return fetch(this.requestBokyRefs)
               // import(/* webpackChunkName: "bokyRefs" */'../data/bokyRefs.json')
                  //       fetch(this.requestBokyRefs)
                     // .then(url => fetch(url.default))
                     .then(res => res.json())
                  .then((obj: BokyRefs[]) => {
                     this.metricsSvc.sendTiming({
                        name: 'bokyRefs',
                        value: this.metricsSvc.timeEnd('fetchAndParsebokyRefs'),
                        event_category: 'JS perfs',
                        event_label: 'fetch_and_parse'
                     });
                     this.bokyRefs = obj;
                     return this.bokyRefs;
                  });
            });
         }
      }
   }

   getBaiboly(): Observable<BaibolyJson> {
      return this._fetchBaiboly$();
   }
   getBible(): Observable<BaibolyJson> {
      return this._fetchBible$();
   }

   getBookRefs() {
      return this._fetchBokyRef$();
   }
}