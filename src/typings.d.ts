declare module '*.json' {
   const content: any;
   export default content;
}
declare module '*.html' {
   const content: any;
   export default content;
}

declare module '*.scss' {
   const content: any;
   export default content;
}

declare var require: {
   <T>(path: string): T;
   (paths: string[], callback: (...modules: any[]) => void): void;
   ensure: (paths: string[], callback: (require: <T>(path: string) => T) => void) => void;
}

declare type Reveal = RevealStatic & {
   getSlides: () => HTMLElement[];
   configure: (options: RevealOptions) => void;
   isReady: () => boolean;
}
declare var Reveal: Reveal;
interface MyWindow extends Window {
   Reveal: Reveal ;
}
interface RevealOptions {
   keyboardCondition?: null | ((event?: KeyboardEvent) => boolean);
   autoPlayMedia?: null | boolean;
   display?: string;
   // Determines where controls appear, "edges" or "bottom-right"
   controlsLayout?: 'bottom-right' | 'edges';
}

declare namespace NodeJS {
   interface ProcessEnv {
      FANDAHARANA_ORIGIN: string;
      FANDAHARANA_URL: string;
   }
}