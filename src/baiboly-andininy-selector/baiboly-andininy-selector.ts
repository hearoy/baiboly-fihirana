import { observer } from '@material/mwc-base/observer';
import { SingleSelectedEvent, ActionDetail } from '@material/mwc-list/mwc-list-foundation';
import '@material/mwc-list/mwc-list-item';
import { ListItemBase } from '@material/mwc-list/mwc-list-item-base';
import { MenuSurface } from '@material/mwc-menu/mwc-menu-surface';
import '@material/mwc-select';
import { Select } from '@material/mwc-select';
import { css, customElement, html, internalProperty, LitElement, property, query, TemplateResult } from 'lit-element';
import { fromEvent, Observable } from 'rxjs';
import { filter, map, pluck, scan, take, takeUntil, tap, debounceTime } from 'rxjs/operators';
import 'web-animations-js/web-animations.min';
import { BokyRefs, TokoRefs } from '../model/baiboly.model';
import { addEventListenerTool } from '../tools';


let booksPromise: Promise<BokyRefs[]>;

export type VerseRefChangeEvent = CustomEvent<{ newValue: { verseRef: string; chapter: string; book: string } }>

type DropdownBook = {
   id: string;
   label: string;
};

/**
 * `baiboly-andininy-selector` Description
 *
 */
@customElement('baiboly-andininy-selector')
export class BaibolyAndininySelector extends LitElement {

   @property({ type: Object })
   bokyRefs: Observable<BokyRefs[]>;

   @internalProperty()
   private books: BokyRefs[] = [];
   private startingChapter: number;
   private startingVerse: number;
   @property({ type: Boolean, attribute: 'hide-book' })
   hideBook: boolean;
   @property({ type: Object, noAccessor: true })
   set fromVerse(refBeginning: string) {
      if (refBeginning) {
         const [bookId, chapterId, verseId] = refBeginning.split('_');
         this.startingVerse = Number(verseId);
         this.startingChapter = Number(chapterId);
         this.bookId = bookId;
         this.getBooks().then(() =>
            this.chapterId = [bookId,chapterId].join('_')
         );
      } else if (this.hideBook){
         this.startingVerse = 0;
         this.startingChapter = 0;
         this.bookId = null;
         this.chapterId = null;
      }
   }

   @property({ type: String, noAccessor: true })
   set bookId(id: string) {
      if (!this.selectedBook || this.selectedBook.id !== id) {
         this.getBooks()
            .then(books => {
               this.books = books.slice();
               if (this.selectedBook?.id !== id) {
                  this.chapterId = null;
               }
               this.selectedBook = id == undefined ? null : books.find(b => b.id === id);
            });
      }
   }
   get bookId(): string {
      return this.selectedBook?.id ?? '';
   }

   @observer(function(this: BaibolyAndininySelector, newV: BokyRefs, oldV: BokyRefs) {
      if (newV == null) {
         this.chapterId = null;
      } else if (oldV !== newV) {
         if (this.hideBook) {
            this.dropdownChapters = this.selectedBook.chapters.slice((this.startingChapter || 1) - 1);
         } else {
            this.dropdownChapters = this.selectedBook?.chapters;
         }
         if (this.chapterId) {
            this.selectedChapter = this.findChapterInSelectedBook();
         }
      }
   })
   @internalProperty()
   selectedBook: BokyRefs;

   @observer(function(this: BaibolyAndininySelector, newV: TokoRefs | null, oldV: TokoRefs | null) {
      if (oldV !== newV) {
         const dropdownStart = Number(this.chapterId?.split('_')[1]) === this.startingChapter
            ? this.startingVerse : 1;
         this.dropdownAndininy = createArrayOfAndininyIndices(this.selectedChapter?.verses, this.chapterId, dropdownStart);
      }
   })
   @internalProperty()
   selectedChapter: TokoRefs | null;

   private _chapterId: string;

   @property({ type: String })
   set chapterId(id: string | null) {
      if (this._chapterId !== id || id == null) {
         this.verseRef = null;
      }
      this._chapterId = id;
      if (this.selectedBook && (!this.selectedChapter || this.selectedChapter.id !== id)) {
         this.selectedChapter = id == undefined ? null : this.findChapterInSelectedBook();
      }
   }
   get chapterId(): string {
      return this._chapterId;
   }

   @internalProperty()
   private dropdownAndininy: { id: string }[] = [];

   @internalProperty()
   private dropdownChapters: TokoRefs[] = [];

   /** passed to the chapter's and verse's paper-dropdown-menu */
   @property({ type: String, attribute:'horizontal-align' })
   horizontalAlign: string;

   @property({ type: String })
   verseRef: string;

   /** passed to the chapter's and verse's paper-dropdown-menu */
   @property({ type: String })
   verticalAlign: string;

   @query('.search-input')
   searchInput: HTMLInputElement;

   bookComparator = (a: BokyRefs | DropdownBook, b: BokyRefs | DropdownBook) => a?.id === b?.id;

   static get styles() {
      return css`
         :host {
            display: block
         }
         .search-input {
            position: absolute;
            visibility: hidden;
            opacity: 0;
         }
         .select--void {
            min-width: 200px;
            display: inline-block;
         }
         @media screen and (max-width: 420px){
            :host {
               max-width: 400px; // .mdc-select__selected-text has a min-width of 200px
            }
            .select ~ .select {
               margin-top: 5px;
            }
         }
      `;
   }

   render() {
      let bookListItems: TemplateResult[] | TemplateResult;
      let verseListItems: TemplateResult[];
      if (!this.hideBook) {
         bookListItems = this.books.map(book => html`
            <mwc-list-item .value=${book.id}
                           .selected=${book.id === this.bookId}>${book.label}</mwc-list-item>`
         );
      }
      const chapterListItems = this.dropdownChapters?.map((chapter) => {
         const chapterIdx = Number(chapter.id.split('_')[1]);
         return html`
         <mwc-list-item .value=${chapter.id}
                        .selected=${chapter.id === this.chapterId}>${chapterIdx}</mwc-list-item>`;
      });

      verseListItems = this.dropdownAndininy?.map((verse) => {
         const verseIdx = Number(verse.id.split('_')[2]);
         return html`
         <mwc-list-item .value=${verse.id}
                        .selected=${verse.id === this.verseRef}>${verseIdx}</mwc-list-item>`
      });

      return html`
      <input type="text"
             class="search-input">
      <!-- ## Book selector ## -->
      ${this.hideBook
            ? html`<div class="select select--void"></div>`
            : html`<mwc-select label = "Boky"
                  class="select"
                  .wrapFocus=${true}
                  @action=${(evt: CustomEvent<ActionDetail>) => this.onBookSelected(evt)}
                  @opened=${evt => this.onSelectOpened(evt)}
                  @closed=${evt => this.onSelectClosed(evt)}>
         ${bookListItems}
      </mwc-select>`}

      <!-- ## Chapter selector ## -->
      <mwc-select label="Toko"
                  class="select"
                  .wrapFocus=${true}
                  @action=${(evt: CustomEvent<ActionDetail>) => this.onChapterSelected(evt)}
                  @opened=${evt => this.onSelectOpened(evt)}
                  @closed=${evt => this.onSelectClosed(evt)}>
         ${chapterListItems}
      </mwc-select>

      <!-- ## Verse selector ## -->
      <mwc-select label="Andininy"
                  class="select" 
                  .wrapFocus=${true}
                  @selected=${(evt: SingleSelectedEvent) => this.onVerseSelected(evt)}
                  @opened=${evt => this.onSelectOpened(evt)}
                  @closed=${evt => this.onSelectClosed(evt)}>
         ${verseListItems}
      </mwc-select>
		`;
   }

   constructor() {
      super();
   }

   connectedCallback() {
      super.connectedCallback();
      this.getBooks();
   }

   private getBooks(): Promise<BokyRefs[]> {
      if (this.books?.length === 0) {
         return booksPromise ?? (booksPromise = this.bokyRefs.pipe(
            tap(() => this.books = []),
            filter(books => books?.length > 0),
            take(1),
            tap({
               next: books => {
                  this.books = books.slice();
               },
               error: () => { },
               complete: () => { }
            }),
         ).toPromise());
      } else {
         return Promise.resolve(this.books);
      }
   }

   private findChapterInSelectedBook() {
      return this.selectedBook?.chapters.find(c => c.id === this.chapterId);
   }

   updated(changedProperties: Map<string | number | symbol, unknown>) {
      if (changedProperties.has('verseRef')) {
         this.dispatchEvent(
            new CustomEvent('verse-ref-changed', {
               bubbles: true,
               detail: {
                  newValue: {
                     verseRef: this.verseRef?.slice(),
                     book: this.bookId,
                     chapter: this.chapterId?.slice(),
                  }
               },
            }) as VerseRefChangeEvent
         )
      }
   }

   onSelectOpened(evt: Event) {
      Reveal.configure({
         keyboardCondition: () => false,
      });
      this.enableItemSearch(evt);
   }

   private enableItemSearch(evt: Event) {
      const select = evt.currentTarget as Select;
      const selectClosed = fromEvent(select, 'closed').pipe(take(1));

      /** stores the last match to deactivate it when select is closed */
      let lastMatch: ListItemBase;
      /** true when a virtual keyboard is sprung up */
      let inputFocused = false;

      /** For virtual keyboards to srping out, a click event on an input is necessary */
      const clickListener = addEventListenerTool(select, 'click', evt => {
         if (evt.target !== evt.currentTarget) {
            return;
         }
         evt.stopPropagation();
         this.searchInput.style.visibility = 'visible'; // needed for the virual keyboard to appear
         this.searchInput.focus();
         this.searchInput.setSelectionRange(0, 0);
         select.layout();
         inputFocused = true;
      });

      fromEvent<KeyboardEvent>(document, 'keyup').pipe( // keydown doesn't see all key events
         pluck('key'),
         /** reimplement a basic key to string transformer */
         map(key => {
            let token = this.searchInput.value;
            if (key === 'Backspace') {
               return token.length > 0 ? token.slice(0, -1) : token;
            }
            if (key.length === 1) {
               return token += key;
            } else { // ignore events whose key is complex
               return token;
            }
         }),
         /** lowercase the token */
         map((token: string) => {
            if (inputFocused) {
               return this.searchInput.value.toLowerCase();
            } else {
               return this.searchInput.value = token?.toLowerCase() ?? '';
            }
         }),
         debounceTime(100),
         /** find a matching item, and deactivate the previous match */
         scan((previousMatch, token) => {
            const matchingItem = select.items.find(item => item.innerText?.replace(/ +/g, '').toLowerCase().startsWith(token));
            if (previousMatch) {
               previousMatch.activated = false;
            }
            return matchingItem;
         },
            null as ListItemBase),
         /** reset the token if there's no match */
         tap(matchingItem => {
            if (!matchingItem) {
               this.searchInput.value = '';
            }
         }),
         takeUntil(selectClosed),
      ).subscribe({
         next: matchingItem => {
            if (matchingItem) {
               matchingItem.focus(); // scrolls the item into the viewport
               matchingItem.activated = true;
               lastMatch = matchingItem;
               if (inputFocused) {
                  matchingItem.scrollIntoView(); // virtual keyboards can still hide an item in the viewport, so scroll it further upward
                  this.searchInput.focus(); // restore the focus in the input so the virtual keyboard stays up
               }
            }
         },
         complete: () => {
            if (lastMatch) {
               lastMatch.activated = false;
            }
            this.searchInput.value = '';
            clickListener.removeNow();
            if (inputFocused) {
               this.searchInput.style.visibility = 'hidden';
            }
         }
      });
   }

   onSelectClosed(evt: Event) {
      Reveal.configure({
         keyboardCondition: null,
      })
   }

   onBookSelected(evt: CustomEvent<ActionDetail>) {
      this.bookId = this.getSelectedValueOrNull(evt);
   }

   onChapterSelected(evt: CustomEvent<ActionDetail>) {
      this.chapterId = this.getSelectedValueOrNull(evt);
   }

   onVerseSelected(evt: CustomEvent<ActionDetail>) {
      this.verseRef = this.getSelectedValueOrNull(evt);
   }

   private getSelectedValueOrNull(evt: CustomEvent<ActionDetail>) {
      const select = evt.currentTarget as Select;
      const indexSelected = evt.detail.index;
      const itemValue = indexSelected > -1 ? select.items[indexSelected].value : null;
      return itemValue;
   }
}

function createArrayOfAndininyIndices(versesLength: number, chapterId: string, startVerse = 1): { id: string; }[] {
   if (versesLength == undefined || chapterId == undefined) {
      return [];
   }
   const startIndex = startVerse - 1;
   return new Array(versesLength - startIndex)
      .fill(0)
      .map((_, idx) => ({ id: computeVerseRef(chapterId, idx + startIndex) }));
}

function computeVerseRef(chapterId: string, idx: number): string {
   return [chapterId, idx + 1].join('_');
}

