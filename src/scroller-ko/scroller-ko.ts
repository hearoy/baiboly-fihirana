import { customElement, property, LitElement, html } from 'lit-element';
import { fromEvent, Subscription } from 'rxjs';
import '@material/mwc-icon-button';
import { addEventListenerTool } from 'src/tools';

declare global {
   interface HTMLElementTagNameMap {
      'scroller-ko': Scroller_ko;
   }
}

export interface ScrollableKo {
   scrollDown: () => void;
   scrollUp: () => void;
}

@customElement('scroller-ko')
export class Scroller_ko extends LitElement {
   /** cache to prevent layout thrashing by repetitively querying the DOM */
   cache: { remaining: number, clientHeight: number, scrollHeight: number, scrollTop: number }

   @property({ type: Boolean, attribute: 'bind-to-keypress' })
   bindToKeypress = false;
   
   @property({ type: Object })
   elt: HTMLElement;

   /** to evaluate if measures should be read from cache or from DOM, to prevent layout thrashing  
    * DISCLOSURE : might not be efficiently implemented 
    */
   hasScrolled: boolean;
   keydownSubsription: Subscription;

   @property({ type: Number, attribute: 'font-size' })
   set fontSize(size: number) {
      this._adjustReadMargin(this._fontSize, size);
      this._fontSize = size || 16;
   }
   get fontSize() {
      return this._fontSize;
   }

   @property()
   set queryElt(q: string) {
      this.elt = document.querySelector(q) || this;
      document.addEventListener('DOMContentLoaded', (evt) => {
         this.elt = document.querySelector(q) || this;
      })
   };

   _fontSize: number;

   /** last time the scroller listener was effective, used to throtle the eventListener */
   timer: number;

   READ_MARGIN: number;

   render() {
      return html`
         <style>
         :host {
            display: flex;
            flex-flow: column nowrap;
            justify-content: space-around;
            align-items: stretch;
            position: absolute;
            bottom: 0;
            right: 0;
            top: 0;
            pointer-events: none;
            z-index: 1;
         }
         .button-wrapper {
            flex: 1 0 0;
            display: flex;
            flex-flow: column;
            justify-content: center;
            min-height: 0; /* otherwise, the flex children overflow their parent */
         }
         #up, #down {
            --mdc-icon-button-size: 32px;
            pointer-events: auto;
         }
         .mid-spacer {
            flex: 0 0 15%;
         }
      </style>

      <div class="button-wrapper">
         <mwc-icon-button id="up">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
               <path d="M7.41 15.41L12 10.83l4.59 4.58L18 14l-6-6-6 6z">
            </svg>
         </mwc-icon-button>
      </div>
      <div class="mid-spacer"></div>
      <div class="button-wrapper">
         <mwc-icon-button id="down">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
               <path d="M7.41 7.84L12 12.42l4.59-4.58L18 9.25l-6 6-6-6z">
            </svg>
         </mwc-icon-button>
      </div>
      `;
   }

   constructor(elt?: HTMLElement) {
      super();
      this.hasScrolled = true;
      if(elt) {
         this.elt = elt;
         this.elt.addEventListener('scroll', this._throttle(this._scrollListener.bind(this)));
      } else {
         this.elt = this as HTMLElement;
      }
      this.cache = { clientHeight: 0, remaining: 0, scrollHeight: 0, scrollTop: 0 };
      this.timer = new Date().getTime();
   }

   firstUpdated() {
      addEventListenerTool(this.shadowRoot!.querySelector('#up')!, 'click', this.scrollUp.bind(this))
      .untilDisconnected(this);
      addEventListenerTool(this.shadowRoot!.querySelector('#down')!, 'click', this.scrollDown.bind(this))
      .untilDisconnected(this);
      this._adjustReadMargin(0, this.fontSize);
   }
   connectedCallback() {
      super.connectedCallback();
      this._bindToKeydown();
   }

   disconnectedCallback() {
      super.disconnectedCallback();
      this.keydownSubsription.unsubscribe();
   }

   _adjustReadMargin(oldV: number, newV: number) {
      this.READ_MARGIN = (newV || 1) * 2 * 1.3;
   }

   _clientHeight() {
      if(this.hasScrolled) {
         return this.cache.clientHeight = this.elt.clientHeight;
      }
      else {
         return this.cache.clientHeight;
      }
   }
   _scrollHeight() {
      if(this.hasScrolled) {
         return this.cache.scrollHeight = this.elt.scrollHeight;
      }
      else {
         return this.cache.scrollHeight;
      }
   }
   _scrollTop() {
      if(this.hasScrolled) {
         return this.cache.scrollTop = this.elt.scrollTop;
      }
      else {
         return this.cache.scrollTop;
      }
   }

   _bindToKeydown() {
      this.keydownSubsription = fromEvent<KeyboardEvent>(window, 'keydown')
         .subscribe(
            evt => {
               const key = evt.key;
               if(this.bindToKeypress) {
                  if(key === 'PageDown') {
                     evt.preventDefault();
                     this.scrollDown();
                  } else if(key === 'PageUp') {
                     evt.preventDefault();
                     this.scrollUp();
                  }
               }
            }
         );
   }

   remaining() {
      if(this.hasScrolled) {
         return this.cache.remaining = this._scrollHeight() - (this._clientHeight() + this._scrollTop())
      } else {
         return this.cache.remaining;
      }
   }

   scrollDown() {
      /** what remains to scroll is smaller than the viewing window */
      if(this.remaining() < this._clientHeight()) {
         const newScroll = this._scrollTop() + this.remaining();
         this._scrollTo(newScroll);
      } else if(this.remaining() == 0) { /** we've done all the scrolling possible */
      } else { /** more than 1 full scroll remaining, so juste scroll to the next "page" minus some margin for readability */
         const newScroll = this._scrollTop() + this._clientHeight() - this.READ_MARGIN;
         this._scrollTo(newScroll);
      }
      // this.hasScrolled = false;
   }

   scrollUp() {
      if(this._scrollTop() > this._clientHeight()) { /** more than 1 full scroll remaining, so just scroll to the next "page" minus some margin for readability */
         const newScroll = this._scrollTop() - this._clientHeight() + this.READ_MARGIN;
         this._scrollTo(newScroll);
      } else if(this._scrollTop() == 0) {
      } else {
         const newScroll = 0;//this._scrollTop();
         this._scrollTo(newScroll);
      }
      // this.hasScrolled = false;
   }

   _scrollListener(evt: UIEvent) {
      const elt = evt.currentTarget;

      if(elt) {
         return this.hasScrolled = true;
      }
      return false;
   }

   _scrollTo(top: number) {
      this.elt.scrollTo({ behavior: 'smooth', top });
      this.hasScrolled = true;
      this.cache.scrollTop = top;
   }

   _throttle(func: ((_?: any) => any)) {
      return (evt: UIEvent) => {
         const now = new Date().getTime();
         if(now - this.timer > 100) {
            this.timer = now;
            func(evt);
         }
      }
   }
}