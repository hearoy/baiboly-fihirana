function findDeep(element: HTMLElement, selector: string) {
   // FIXME with returnDOMNodes: true, element is an array but types don't reflect that
   if (Array.isArray(element)) {
      element = element[0];
   }
   const elt = kagekiri.querySelector(selector, element);
   return elt;
}

function findAllDeep(element: HTMLElement, selector: string) {
   if (Array.isArray(element)) {
      element = element[0];
   }
   return kagekiri.querySelectorAll(selector, element);
}

export function augmentWithDeepFind(selector: Selector | SelectorPromise): SelectorWithDeepQs {
   return selector.addCustomMethods(
      {
         findDeep,
         findAllDeep,
      },
      {returnDOMNodes: true}
   ) as SelectorWithDeepQs;
}
