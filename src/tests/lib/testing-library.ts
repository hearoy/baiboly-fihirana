import * as kagekiri from 'kagekiri';
// import * as querySelectorShadowDom from 'query-selector-shadow-dom';
// declare namespace querySelectorShadowDom {
//    function querySelectorAllDeep(selector: string): NodeList;
//    function querySelectorDeep(selector: string): HTMLElement;
// }
// HTMLElement.prototype.querySelectorAll = querySelectorShadowDom.querySelectorAllDeep;
// HTMLElement.prototype.querySelector = querySelectorShadowDom.querySelectorDeep;
// document.querySelector = querySelectorShadowDom.querySelectorDeep;
// document.querySelectorAll = querySelectorShadowDom.querySelectorAllDeep;
declare namespace kagekiri {
   function querySelectorAll(selector: string): NodeList;
   function querySelector(selector: string): HTMLElement;
}
// HTMLElement.prototype.querySelectorAll = kagekiri.querySelectorAll;
// HTMLElement.prototype.querySelector = kagekiri.querySelector;
// document.querySelector = kagekiri.querySelector;
// document.querySelectorAll = kagekiri.querySelectorAll;

window['kagekiri'] = kagekiri;