import esbuild from 'esbuild';
import * as path from 'path';
import createTestcafe from 'testcafe';
import * as qrcode from 'qrcode-terminal';
import {loadConfig, register} from 'tsconfig-paths';

declare interface QRCodeTerminal {
  generate(source: string, {small: boolean}?, cb?: (qr: string) => void): void;
  setErrorLevel(level: 'L' |'M' | 'Q' | 'H'): void;
}
// declare const qrcode: QRCodeTerminal;

const BASE_PATH = path.dirname(process.argv[1]);

esbuild
  .build({
    bundle: true,
    color: true,
    entryPoints: [`${BASE_PATH}/lib/testing-library.ts`],
    format: 'iife',
    minifySyntax: true,
    minifyWhitespace: true,
    outdir: `${BASE_PATH}/out/lib`,
    platform: 'browser',
    splitting: false,
    tsconfig: `${BASE_PATH}/lib/tsconfig.json`,
  })
  .then(() => createTestcafe(null, 21321, 21322))
  .then(
    (testcafe) =>
      new Promise<{testcafe: TestCafe; browserConnection: BrowserConnection}>(
        (resolve) => {
          const browserConnection = testcafe.createBrowserConnection();
          browserConnection.then((c) => {
            process.stdin.write(c.url + '\n');
            qrcode.generate(c.url);
            c.once('ready', () => resolve({testcafe, browserConnection: c}));
          });
        }
      )
  )
  .then(({testcafe, browserConnection}) => {
    const runner = testcafe.createLiveModeRunner();
    const config = loadConfig(`${BASE_PATH}/tsconfig.ui.json`);
    if (config.resultType === 'success') {
      register(config);
    }
    return runner
      .browsers(browserConnection)
      .clientScripts([
        {
          path: `${BASE_PATH}/out/lib/testing-library.js`,
        },
        {
          module: `@testing-library/dom/dist/@testing-library/dom.umd.js`,
        },
      ])
      .reporter('spec', process.stdout)
      .src(`${BASE_PATH}/**/*.ui.ts`)
      .tsConfigPath(`${BASE_PATH}/tsconfig.ui.json`)
      .run({
        debugOnFail: true,
      });
  });
