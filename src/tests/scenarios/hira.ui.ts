/// <reference path="../testing-library.d.ts" />

import {screen} from '@testing-library/testcafe';
import {ClientFunction, Selector} from 'testcafe';
import { augmentWithDeepFind } from '../helpers/selectors';

declare const evalElt: () => HTMLElement;
declare const evalElts: () => HTMLElement[];

const BASE_URL = `https://localhost:9000`;

fixture`Hira`;

const getUrl = ClientFunction(() => location.href);
const getSearchParam = ClientFunction((key: string) =>
   decodeURIComponent(new URL(location.href).searchParams.get(key))
);
const qsDeep = Selector((selector: string) => kagekiri.querySelector(selector));
const qsAllDeep = Selector((selector: string) =>
   kagekiri.querySelectorAll(selector)
);

const hiraSlide = augmentWithDeepFind(qsDeep('section.present hr-hira'));

test.page(BASE_URL)('Sets up hira from scratch', async (t) => {
   await t
      .expect(screen.queryByAltText('FPMA Orléans').visible)
      .ok()
      .expect(getUrl())
      .contains('accueil-logo')
      .wait(500)
      .pressKey('right')
      .expect(
         screen
            .queryAllByText('Fitadiavana tononkira', {
               selector: '.section__title',
            })
            .filterVisible().count
      )
      .eql(1)
      .pressKey('right')
      .expect(
         screen.queryByText('Mifidy vakiteny', {selector: '.section__title'})
            .visible
      )
      .ok()
      .pressKey('right')
      .expect(
         screen.queryByText('Mifidy ny hira', {selector: '.section__title'})
            .visible
      )
      .ok()
      .typeText(
         qsDeep('section.present paper-input#input input'),
         '18:1-2, ff26, an5:1, fp1'
      )
      .click(qsDeep('section.present paper-button#processButton'))
      .expect(getSearchParam('hiras'))
      .eql('ffpm_18:1-2,ff_26,an_5:1,fp_1');

   for (const {title, expectedAndininy} of [
      {title: '18', expectedAndininy: [1, 2]},
      {title: 'FF 26', expectedAndininy: []},
      {title: 'Antema 5', expectedAndininy: [1]},
      {title: 'Fanekem-pinoana 1', expectedAndininy: []},
   ]) {
      await checkHiraSlide({t, title, expectedAndininy});
      await t.pressKey('right');
   }
   await t.pressKey('left');
   await t.eval(() => location.reload());
   await checkHiraSlide({
      t,
      timeout: 10000,
      title: 'Antema 5',
      expectedAndininy: [1],
   });
});

test('Add hiras', async (t) => {
   await checkHiraSlide({
      expectedAndininy: [2, 3],
      t,
      title: 'FF 34',
      timeout: 10000,
   });
   const hiraManagerInDialog = augmentWithDeepFind(qsDeep('recherche-hira hira-manager'));
   await t
      .pressKey('left')
      .click('.custom-buttons__icon.recherche-button')
      .expect(hiraManagerInDialog.visible)
      .ok()
      .typeText(
         hiraManagerInDialog.findDeep('paper-input input'),
         '813:2-3-4, ff31, 351:1-3'
      )
      .click(hiraManagerInDialog.findDeep('paper-button#processButton'));
   for (const {title, expectedAndininy} of [
      {title: '813', expectedAndininy: [2, 3, 4]},
      {title: 'FF 31', expectedAndininy: []},
      {title: '351', expectedAndininy: [1,3]},
   ]) {
      await checkHiraSlide({t, title, expectedAndininy});
      await t.pressKey('right');
   }
}).page(
   `${BASE_URL}/?hiras=ffpm_701%3A2%2Cfp_2%2Can_2%3A1%2Cff_34%3A2-3&pos=7#/7`
);

async function checkHiraSlide({
   t,
   title,
   expectedAndininy,
   timeout,
}: {
   t: TestController;
   title: string;
   expectedAndininy: number[];
   timeout?: number;
}) {
   const selector = timeout
      ? (hiraSlide.with({timeout}) as SelectorWithDeepQs)
      : hiraSlide;
   await t.expect(selector.findDeep('.title').textContent).contains(title);
   await checkSelectedAndininy(hiraSlide, t, expectedAndininy);
}

async function checkSelectedAndininy(
   hiraSlide: SelectorWithDeepQs,
   t: TestController,
   expected: number[]
) {
   const andininy = hiraSlide.findAllDeep(
      '.lyrics__verse-container:not(.andininy--refrain)'
   );
   const selectedIndices = [];
   for (let i = 0; i < (await andininy.count); i++) {
      const andininyClassNames = await andininy.nth(i).classNames;
      if (andininyClassNames.includes('lyrics__verse-container--selected')) {
         selectedIndices.push(i + 1);
      }
   }
   await t.expect(selectedIndices).eql(expected);
}
