import { Fihirana } from 'fihirana-reveal/lib/component/hr-hira/hr-hira';
import { Observable, of, defer } from 'rxjs';

let cached: Fihirana.FihiranaJson;
export function getFihirana(): Observable<Fihirana.FihiranaJson> {
   if (cached) {
      return of(cached);
   } else {
      return defer(() => {
         if (cached) {
            return of(cached);
         } else {
            return fetch('/data/fihirana.json')
               .then(res => res.json())
               .then(obj => cached = obj);
         }
      });
   }
}