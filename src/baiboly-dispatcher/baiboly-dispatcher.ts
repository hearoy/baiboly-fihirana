import '@material/mwc-button';
import '@material/mwc-linear-progress';
import { Snackbar } from '@material/mwc-snackbar';
import { css, customElement, html, internalProperty, LitElement, property, query } from 'lit-element';
import { nothing } from 'lit-html';
import { cache } from 'lit-html/directives/cache';
import { asyncScheduler, BehaviorSubject, defer, EMPTY, forkJoin, from as from$, Observable, of, scheduled } from 'rxjs';
import { bufferCount, concatMap, distinctUntilChanged, filter, first, groupBy, map, mergeMap, reduce, switchMap, take, tap } from 'rxjs/operators';
import { ClearSlideEvent, CLEAR_SLIDE_EVENT } from 'src/model/slide';
import { addEventListenerTool, fromEventOnce, Tools, untilDisconnected } from 'src/tools';
import '../baiboly-andininy-selector/baiboly-andininy-selector';
import type { VerseRefChangeEvent } from '../baiboly-andininy-selector/baiboly-andininy-selector';
import { BaibolyPuller } from '../baiboly-puller/baiboly-puller';
import { acknowledgeMessage, answerWithCorrection, getChannelBaiboly } from '../inter-communication/intercom';
import { Andininy, AndininyJson, BaibolyJson, BaibolySequenceItem, BokyJson, BokyRefs, TokoJson } from '../model/baiboly.model';
import { MetricsSvc } from '../tools/metrics';

export type BookRefStripped = { id: String, label: String };



declare global {
	interface HTMLElementTagNameMap {
		'baiboly-dispatcher': BaibolyDispatcher;
	}
}
@customElement('baiboly-dispatcher')
export class BaibolyDispatcher extends LitElement {

	@property({ type: Object })
	baiboly: Observable<BaibolyJson[]>;

	private bokyRefs: Observable<BokyRefs[]>;
	private bokyRefsDownloaded: boolean;

	@property({ type: String })
	bookFrom: string;

	@property({ type: String })
	chapterFrom: string;
	@property({ type: String })
	chapterTo: string;

	@property({ type: Array })
	sequences: BaibolySequenceItem[][];

	/**
	 * @description Andininy.id of the sequence's start
	 * @type {String}
	 * @memberof BaibolyDispatcher
	 */
	@property({ type: String })
	from: string;

	@property({ type: Object })
	selectedBook: HTMLElement;

	/**
	 * @description Andininy.id of the sequence's end
	 * @type {String}
	 * @memberof BaibolyDispatcher
	 */
	@property({ type: String })
	to: string;
	private metricsSvc: MetricsSvc;

	@query('.snackbar')
	snackbar: Snackbar;

	/** should not be set except by `processing` */
	@internalProperty()
	private hideProgressBar: boolean;
	private _processing = new BehaviorSubject(false);
	readonly processing = this._processing.pipe(distinctUntilChanged());
	invalid = false;

	@internalProperty()
	private showAndininySelectors: boolean;
	private dispatcherIndex: number;
	private setByMessage: boolean;

	static get styles() {
		return css`
			:host {
            display: flex;
            flex-direction: row;
            flex-flow: row nowrap;
            justify-content: center;
         }
         .wrapper {
            display: flex;
            flex-flow: column;
            align-items: center;
         }
         baiboly-andininy-selector:first-of-type {
            margin-bottom: 1rem;
         }
         .title {
            font-size: 1rem;
            align-self: flex-start;
            margin-bottom: 0.3rem;
         }
         @media screen and (max-width: 420px){
            .title {
               margin-left: 1rem;
            }
            .title:first-of-type {
               margin-top: 0;
            }
         }


			:host .buttons {
				display: flex;
				flex-flow: row nowrap;
				justify-content: center;
				width: 100%;
				margin-top: 2rem;
			}
			:host mwc-button ~ mwc-button {
				margin-left: 1rem;
			}

			:host mwc-button {
			transition: background-color 0.3s ease;
			}

			:host mwc-button:hover {
			background-color: rgba(38, 73, 147, .2);
			}

			.button-process {
			text-transform: none;
			flex: 0 0 40%;
			width: 50%;
			max-width: 10rem;
			}
			.button-clear {
				text-transform: none;
			}

			:host mwc-button.button-process:hover {
			background-color: #264993;
			}
			.progressbar {
				position: absolute;
				top: 0;
				width: 100%;
				--mdc-theme-primary: var(--fpmao-manga, blue);
			}
		`;
	}

	render() {
		return html`
      <div class="wrapper">

			<h3 class="title">Manomboka...</h3>
			${cache(this.showAndininySelectors || this.bokyRefsDownloaded
			? html`
				<baiboly-andininy-selector .bokyRefs=${this.bokyRefs} 
												horizontal-align="center"
												.verseRef=${this.from} 
												@verse-ref-changed=${evt => this.startOfSequenceSelected(evt)} 
												.bookId=${this.bookFrom} 
												.chapterId=${this.chapterFrom}></baiboly-andininy-selector>
         <h3 class="title">...ka hatramin'ny</h3>
			<baiboly-andininy-selector .verticalAlign=${'bottom'}
												hide-book
												.fromVerse=${this.from}
												horizontal-align="center"
												.bokyRefs=${this.bokyRefs}
												@verse-ref-changed=${evt => this.endOfSequenceSelected(evt)}></baiboly-andininy-selector>
				`
			: nothing)}
			
         <div class="buttons">
				<mwc-button class="button-clear"
								@click=${this.onDeleteClick.bind(this)}>Fafàna</mwc-button>
				<mwc-button class="button-process"
								@click=${this.onProcessClick.bind(this)}
								raised>Ampiana</mwc-button>
			</div>
		</div>
		<mwc-snackbar class="snackbar"
						  timeoutMs=5000>
			${cache(this.invalid ? nothing : html`<mwc-button slot="action">Mankany</mwc-button>`)}
		</mwc-snackbar>
		<mwc-linear-progress indeterminate
									class="progressbar"
									.closed=${this.hideProgressBar}></mwc-linear-progress>
		`;
	}
	endOfSequenceSelected(evt: VerseRefChangeEvent) {
		const { book, chapter, verseRef } = evt.detail.newValue;
		this.chapterTo = chapter;
		this.to = verseRef;
	}

	startOfSequenceSelected(evt: VerseRefChangeEvent) {
		const { book, chapter, verseRef } = evt.detail.newValue;
		this.bookFrom = book;
		this.chapterFrom = chapter;
		this.chapterTo = chapter;
		this.from = verseRef;
	}


   /**
    * Instance of the element is created/upgraded. Use: initializing state,
    * set up event listeners, create shadow dom.
    * @constructor 
    */
	constructor() {
		super();
		this.metricsSvc = new MetricsSvc();
		this.sequences = [];
		this.processing.pipe(
			untilDisconnected(this),
		).subscribe(pending => this.hideProgressBar = !pending);
	}

	disconnectedCallback() {
		super.disconnectedCallback();
	}


	protected mutate<T extends keyof this>(name: T, mutation: (old: this[T]) => this[T]) {
		let oldVal = this[name];
		//@ts-ignore
		oldVal = oldVal instanceof Array ? oldVal.slice() : oldVal;
		mutation(this[name]);
		return this.requestUpdate(name, oldVal);
	}

	connectedCallback() {
		super.connectedCallback();
		this.downloadRessources();
		const slide = this.closest('section');
		this.showAndininySelectors = slide.classList.contains('present');
		this.listenToSlideChangesUntilBokyRefsDownloaded(slide);
		if (Reveal.isReady()) {
			this.dispatcherIndex = Reveal.getIndices(this.closest('section')).h;
		} else {
			fromEventOnce('ready', Reveal as any).then(() => {
				this.dispatcherIndex = Reveal.getIndices(this.closest('section')).h;
			});
		}
	}

	/**
	 * Prevent stamping `baiboly-andininy-selector` early to make bokyRefs's request lazy.
	 * When bokyRefs has been downloaded, leave `baiboly-andininy-selector` stamped and no longer listen to slide changes.
	 */
	private listenToSlideChangesUntilBokyRefsDownloaded(slide: HTMLElement) {
		addEventListenerTool(Reveal as any, 'slidechanged', (event: Event & {
			currentSlide: HTMLElement;
		}, removeNow) => {
			this.showAndininySelectors = event.currentSlide === slide;
			if (this.showAndininySelectors) {
				const removeListernerWhenComplete = () => this.updateComplete.then((complete) => {
					if (complete) {
						removeNow();
					}
					else {
						removeListernerWhenComplete();
					}
				});
				removeListernerWhenComplete();
			}
		});
	}

   /**
    * Use for one-time configuration of your component after local DOM is initialized. 
    */
	firstUpdated(changedProperties: Map<string | number | symbol, unknown>) {
		super.firstUpdated(changedProperties);
		this.subscribeToBaibolyTopic();
		addEventListenerTool(window, CLEAR_SLIDE_EVENT, this.clearSlide.bind(this), { passive: true }).untilDisconnected(this);
	}

	updated(changedProperties: Map<string | number | symbol, unknown>) {
		super.updated(changedProperties);
		if (changedProperties.has('sequences')) {
			this.dispatchEvent(
				new CustomEvent('sequenceschanged', {
					composed: true,
					bubbles: true,
					detail: { newValue: this.sequences },
				})
			);

		}
	}

	downloadRessources() {
		const bp = new BaibolyPuller();
		this.baiboly = forkJoin([bp.getBaiboly(), bp.getBible()]);
		this.bokyRefs = bp.getBookRefs().pipe(
			tap(() => { this.bokyRefsDownloaded = true; }),
		);
	}

   /**
    * @description Takes a starting and an ending reference well-formatted
    *  and returns an array containing the references of all the verses in-between
    * @param {String} from formatted as Book_Chapter_Verse
    * @param {String} to Book_Chapter_Verse
    * @returns {Observable<String[]>} the sequence of verses as a sequence of references
    * @memberof BaibolyDispatcher
    */
	expandRangeExplicitly(from: string, to: string): Observable<string[]> {
		const [bookF, chapterF, versesF] = from.split('_').map((val, idx) => idx === 0 ? val : Number(val)) as [string, number, number];
		const [bookT, chapterT, versesT] = to.split('_').map((val, idx) => idx === 0 ? val : Number(val)) as [string, number, number];
		const chapterFId = [bookF, chapterF].join('_');
		const chapterTId = [bookT, chapterT].join('_');
		let sequence$: Observable<string[]>;
		let sequence: string[] = [];
		if (chapterF === chapterT) { // if it stays in the same chapter
			const start = versesF;
			const end = versesT;
			for (let i = start; i <= end; i++) {
				sequence.push(
					[chapterFId, i].join('_')
				);
			}
			sequence$ = of(sequence);
		} else { // same book, different chapters
			sequence$ = this.bokyRefs.pipe(
				switchMap(array => from$(array))
				// ).pipe(
				//    filter(boky => {
				//       return boky.id == bookF || boky.id == bookT;
				//    }),
				//    take(2)
				// )
			).pipe(
				first(boky => boky.id == bookF),
				switchMap(boky => from$(boky.chapters)),
				filter(chapter => {
					const chapterNumber = Number(chapter.id.split('_')[1]);
					return chapterNumber >= chapterF && chapterNumber <= chapterT;
				}),
				take(chapterT - chapterF + 1),
			).pipe(
				reduce<{ id: string, verses: Number }, string[]>((acc, chapterRefs) => {
					const chapterNumber = Number(chapterRefs.id.split('_')[1]);
					if (chapterRefs.id == chapterFId) {
						/** the starting chapter so start from the given verse until the last of the chapter */
						for (let i = Number(versesF); i <= chapterRefs.verses; i++) {
							acc.push(
								[chapterRefs.id, i].join('_')
							);
						}
					} else if (chapterNumber < chapterT) {
						/** in-between chapter so start from the beginning of the chapter to the end */
						for (let i = 1; i <= chapterRefs.verses; i++) {
							acc.push(
								[chapterRefs.id, i].join('_')
							);
						}

					} else {
						/** ending chapter so start from the beginning of the chapter until the given verse */
						for (let i = 1; i <= Number(versesT); i++) {
							acc.push(
								[chapterRefs.id, i].join('_')
							);
						}
					}
					return acc;
				}, [])
			);
		}
		return sequence$;
	}

	clearSlide(event: ClearSlideEvent): Promise<void> {
		const { coords, type } = event.detail;
		if (type === 'baiboly') {
			this.mutate('sequences', sequences => (sequences.splice(coords.v, 1)));
			return this.updateQueryString(0);
		} else {
			return Promise.resolve();
		}
	}

	onDeleteClick(evt: Event) {
		this.mutate('sequences', (sequences) => sequences.splice(0, this.sequences.length));
		this.updateQueryString(0);
	}

	onProcessClick(evt: CustomEvent) {
		this.metricsSvc.sendEvent({
			action: 'click',
			category: 'bible',
			label: 'select bible sequence',
		});
		this._processing.next(true);
		const { from, to } = this;
		if (!from || !to) {
			this.invalid = true;
			this.snackbar.labelText = 'Tsy maintsy fenoina ny boky sy toko ary andininy o!';
			this.snackbar.show();
			whenSnackbarCloses.call(this, (_, removeNow) => {
				this.invalid = false;
				removeNow();
			});
			this._processing.next(false);
			return;
		}
		this.process({ from, to }).subscribe({
			next: () => {
				this._processing.next(false);
				displaySnackbar.call(this);
			},
			error: () => this._processing.next(false),
			complete: () => this._processing.next(false),
		});

		function displaySnackbar(this: BaibolyDispatcher) {
			this.snackbar.labelText = `Voatovana`
			this.snackbar.show();

			whenSnackbarCloses.call(this,
				(event: CustomEvent<{ reason?: 'action' | 'dismiss' | string }>, removeNow) => {
					let hashChanged = false;
					if (event.detail?.reason === 'action') {
						// wait for hashchange or timeout and go to the last sequence added
						const hashChangeListener = addEventListenerTool(window, 'hashchange', (event, removeEventListener) => {
							goToLastlyAddedSequence.call(this);
							removeEventListener();
						});

						setTimeout(() => {
							if (!hashChanged) {
								hashChangeListener.removeNow();
								goToLastlyAddedSequence.call(this);
							}
						}, 500);
					}
					removeNow();
					function goToLastlyAddedSequence() {
						hashChanged = true;
						const { h } = Reveal.getIndices();
						Reveal.slide(h + 1, this.sequences.length - 1);
					}
				});
		}
		function whenSnackbarCloses(callback: (event: Event, removeNow: () => void) => void) {
			addEventListenerTool(this.snackbar, 'MDCSnackbar:closing', callback);
		}
	}

	process({ from, to, measure = true }: { from: string, to: string, measure?: boolean }) {
		return this.expandRangeExplicitly(from, to)
			.pipe(
				tap(() => measure && this.metricsSvc.timeStart('searchInBible')),
				concatMap(refs => this.refs2verses(refs)),
				tap(() => measure && this.metricsSvc.sendTiming({
					name: 'searchInBible',
					value: this.metricsSvc.timeEnd('searchInBible'),
					event_category: 'JS perfs',
					event_label: 'process'
				})),
				map(verseCouple => {
					const obj = verseCouple.map(it => new Andininy(it, it.id.split('_').slice(0, 2).join('_')));
					return obj;
				}),
				map(andininy => {
					return new BaibolySequenceItem(...andininy);
				}),
				reduce<BaibolySequenceItem, BaibolySequenceItem[]>((acc, item) => {
					return [...acc, item];
				}, []),
				concatMap((verses: BaibolySequenceItem[]) => {
					if (verses.length > 0) {
						return this.mutate('sequences', sequences => (sequences.push(verses), sequences))
					} else {
						return EMPTY;
					}
				}),
				tap({
					next: v => {
						this.updateQueryString(0);
					}
				})
			);
	}

	processQueryString() {
		// ?bible=at01_1_1-at01_1_10,
		const searchParams = (new URL(location.href)).searchParams;
		const listOfSequences = searchParams.get('baiboly');
		const position = searchParams.get('pos');
		let title = searchParams.get('title');
		if (title != null && title != 'null')
			title.replace('+', ' ');
		else
			title = document.title;
		if (!listOfSequences)
			return;
		const sequences = listOfSequences.split(',').map(couple => {
			const [from, to] = couple.split('-');
			return { from, to };
		});
		return from$(sequences).pipe(
			concatMap((couple, index) => {
				if (index === 0) {
					this._processing.next(true);
				}
				const { from, to } = couple;
				return this.process({ from, to });
			})
		).pipe(
			tap({
				complete: () => {
					this._processing.next(false);
					this.dispatchEvent(new CustomEvent<void>('baiboly-added', { composed: true, bubbles: true }));
				}
			})
		).toPromise();

	}

   /**
    * @description Takes references to verses and returns the verses in the languages stored in the state
    * @param {string[]} refs references of the verses formatted as Book_Chapter_Verse
    * @memberof BaibolyDispatcher
    */
	refs2verses(refs: string[]): Observable<AndininyJson[]> {
		type Refs = { book: string, chapter: string, verse: string, verseId: string };
		const refsSplit: Refs[] = refs.map(ref => {
			const [book, chapter, verse] = ref.split('_');
			return { book, chapter, verse, verseId: ref };
		});

		const andininy$ = this.baiboly.pipe(
			concatMap(bibles => from$(bibles)),
			concatMap(books => from$(books).pipe(
				filter<BokyJson>(book => {
					return refsSplit.some(ref => ref.book == book.id);
				}),
				first(),
				concatMap(books => from$(books.chapters).pipe(
					filter<TokoJson>(chapter => {
						return refsSplit.some(ref => [ref.book, ref.chapter].join('_') == chapter.id);
					})
				)),
				concatMap((chapters, chIdx) => from$(chapters.verses).pipe(
					filter<AndininyJson>(verse => {
						return refsSplit.some(ref => ref.verseId == verse.id);
					}),
					switchMap((verse, vIdx) => {
						if (vIdx < this.computeNumberOfVersesPerChapter(refsSplit).numVerses[chIdx]) {
							return of(verse);
						} else {
							return EMPTY;
						}
					})
					// take(this.getDifferentChapters(refsSplit).numChapters)
				)),
			)),
			// here we have selected the andininy in one language
			groupBy(item => item.id),
			mergeMap(item => {
				return item.pipe(
					bufferCount(2),
				)
			})
		);

		return andininy$;
	}

	private subscribeToBaibolyTopic() {
		getChannelBaiboly().pipe(
			filter(({ action }) => action === 'go'),
			untilDisconnected(this),
			filter(({ payload }) => payload.match(/((at|nt)\d+_\d+_\d+)-((at|nt)\d+_\d+_\d+)/).length > 0),
			switchMap(message =>
				this.baiboly.pipe(
					take(1),
					switchMap(() => scheduled(defer(() => {
						if (this.sequences?.length) {
							return of(null);
						} else {
							return fromEventOnce('sequenceschanged', this);
						}
					}), asyncScheduler)), // let sequences be filled via baiboly
					take(1),
					map(() => message),
				)
			),
		).subscribe((message) => {
			const { payload: target } = message;
			const vIndex = this.sequences.findIndex(seq => {
				const [first, last] = [seq[0].id, seq[seq.length - 1].id];
				let range = [first, last].join('-');
				return target === range;
			});
			console.log({ vIndex });
			if (vIndex > -1) {
				Reveal.slide(this.dispatcherIndex + 1, vIndex);
				acknowledgeMessage(message);
			} else {
				const [from, to = from] = target.split('-');
				this._processing.next(true);
				this.process({ from, to, measure: false }).subscribe({
					next: () => {
						this._processing.next(false);
						Reveal.slide(this.dispatcherIndex + 1, this.sequences.length - 1);
						answerWithCorrection({ ...message, action: 'add' });
					}
				});
			}
			this.setByMessage = true;
		});
	}

	private computeNumberOfVersesPerChapter(refsSplit: { book: string; chapter: string; verse: string; verseId: string; }[]) {
		return refsSplit.reduce(({ lastChapter, numVerses }, ref) => {
			if (lastChapter !== ref.chapter) {
				numVerses.push(1);
				lastChapter = ref.chapter;
			} else {
				numVerses.push(numVerses.pop() + 1);
			}
			return { lastChapter, numVerses };
		}, { lastChapter: '', numVerses: [] });
	}

	updateQueryString(delay: number): Promise<void> {
		if (delay > 0) {
			return Tools.setTimeout<void>(this.updateQueryString.bind(this, 0), delay);
		}
		const queryString = new URLSearchParams(location.search.slice(1));
		const hash = location.hash;
		// const pattern = /[&?]?baiboly=(((at|nt)\d+_\d+_\d+)-((at|nt)\d+_\d+_\d+),?)+/;
		/* remove all previous sequences or pos in query string to recreate them from the found parameters */
		// let newQueryString = queryString.replace(pattern,'');
		/* get all hiras added and get the list of their hira-id */
		const sequences = this.sequences
			.map(seq => ([seq[0].id, seq[seq.length - 1].id].join('-')))
			.join(',');
		queryString.set('baiboly', sequences);
		window.history.pushState(null, '', location.pathname + '?' + queryString.toString() + hash);
		return Promise.resolve();
	}

	__test() {
		this.expandRangeExplicitly('nt05_10_40', 'nt05_11_3').subscribe({
			// next: arr => staticConcat(this.refs2verses(arr)).subscribe(obs => {
			// 	debugger;
			// 	console.dirxml(obs);
			next: arr => this.refs2verses(arr).subscribe(obs => {
				console.dirxml(obs);
			})
		});
	}

}
