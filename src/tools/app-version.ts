export const version = '5.15.0';

declare global {
  namespace fpmao_fanipazana {
    const version: string;
  }
}

if (!globalThis.fpmao_fanipazana) {
  globalThis.fpmao_fanipazana = {
     version,
   }
}