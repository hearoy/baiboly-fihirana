export const propId = 'UA-157350031-1';

interface GTagEvent {
   /** The type of interaction (e.g. 'play') */
   action: Exclude<string, 'timing_complete'>;
   /** Typically the object that was interacted with (e.g. 'Video') */
   category?: string;
   /** Useful for categorizing events (e.g. 'Fall Campaign') */
   label?: string;
   /** A numeric value associated with the event (e.g. 42) */
   value?: number;
   /** If set to true, the type of event hit is not considered an interaction hit */
   non_interaction?: boolean;
}

interface GTagTiming {
   /** A string to identify the variable being recorded (e.g. 'load'). */
   name: string;
   /** The number of milliseconds in elapsed time to report to Google Analytics (e.g. 20). */
   value: number;
   /** A string for categorizing all user timing variables into logical groups (e.g. 'JS Dependencies'). */
   event_category?: string;
   /** A string that can be used to add flexibility in visualizing user timings in the reports (e.g. 'Google CDN'). */
   event_label?: string;
}

let GTAG_UNAVAILABLE = false;
let GTAG_UNAVAILABLE_WARNING_DISPLAYED = false;

export class MetricsSvc {
   private static instance: MetricsSvc;
   gtag: Gtag.Gtag = function(...args) {
      if (typeof gtag === 'function') {
         // @ts-ignore
         gtag(...args);
      } else {
         GTAG_UNAVAILABLE = true;
      }
      if (GTAG_UNAVAILABLE) {
         if (!GTAG_UNAVAILABLE_WARNING_DISPLAYED) {
            console.warn(`gtag is unavailable`);
            GTAG_UNAVAILABLE_WARNING_DISPLAYED = true;
         }
      }
   }
   constructor() {
      return MetricsSvc.instance || this;
   }

   timeStart(markName: string): void {
      performance.mark(`${markName}_mark`);
   }

   timeEnd(markName: string): number {
      const measureEntry = `${markName}_measure`;
      try {
         performance.measure(measureEntry, `${markName}_mark`);
         return performance.getEntriesByName(measureEntry)[0].duration;
      } catch (e) {
         throw new Error(`No mark named '${markName}_mark' were found`);
      } finally {
         performance.clearMarks(markName);
         performance.clearMeasures(measureEntry);
      }
   }

   sendEvent(event: GTagEvent) {
      const { action, category, label, non_interaction } = event;
      const value = event.value ? Math.round(event.value) : event.value;
      this.gtag('event', action, {
         event_category: category,
         event_label: label,
         value,
         non_interaction
      });
   }

   sendTiming(timing: GTagTiming) {
      const { name, event_category, event_label } = timing;
      const value = timing.value ? Math.round(timing.value) : timing.value;
      this.gtag('event', 'timing_complete', {
         name,
         value,
         event_category,
         event_label,
      });
   }
}