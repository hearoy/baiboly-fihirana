export interface SlideCoords { h: number, v: number };
export interface SlideClearee {
	clearMe(opts: {coords: SlideCoords}): void;
}
export function clearSlideEventFactory(data: ClearSlideData) {
	return new CustomEvent('clear-slide', {
		bubbles: true,
		composed: true,
		detail: data,
	});
}
export type ClearSlideEvent = CustomEvent<ClearSlideData>;
export const CLEAR_SLIDE_EVENT = 'clear-slide';
export type ClearSlideData = { type: 'hira' | 'baiboly', coords: SlideCoords };