import { Andininy, BaibolySequenceItem } from '../model/baiboly.model';


import { Menu } from '@material/mwc-menu';
import '@material/mwc-menu';
import '@material/mwc-ripple';
import '@material/mwc-icon-button';
import '@material/mwc-button';
import '@material/mwc-list';
import { Scroller_ko, ScrollableKo } from '../scroller-ko/scroller-ko';
import { LitElement, customElement, html, property, query, css } from 'lit-element';
import { observer } from '@material/mwc-base/observer';
import { iconFormatSize } from 'src/tools/my-icons';

/**
 * `baiboly-sequence` 
 *
 */
@customElement('baiboly-sequence')
export class BaibolySequence extends LitElement implements ScrollableKo {

  get books(): string[] {
    if (this.verses.length > 0)
      return this.verses[0].bookNames;
    else
      return [];
  }

  @observer(function _updateFontSizeChapter(this: BaibolySequence, newV, oldV) {
    this.style.setProperty('--chapter-font-size', `${this._fontSizeChapter}px`);
  })
  @property({ type: Number })
  _fontSizeChapter: number;

  @observer(function _updateFontSize(this: BaibolySequence, newV, oldV) {
    document.body.style.setProperty('--baiboly-verse__fontSize', `${this._fontSizeVerses}px`);
  })
  @property({ type: Number })
  _fontSizeVerses = Number((document.body.style.getPropertyValue('--baiboly-verse__fontSize') ?? '30px').replace('px', ''));

  mql: MediaQueryList;

  @query('#menu')
  menu: Menu;

  @query('scroller-ko')
  scroller: Scroller_ko;

  @property({ type: String })
  title = 'Vakiteny'

  @property({ type: Array })
  verses: BaibolySequenceItem[];

  static get styles() {
    return css`
    :host {
        display: block;
        height: 100%;
        overflow: auto;
      }

      .book__outer {
        display: flex;
        flex-flow: row;
        flex-wrap: nowrap;
      }

      .book {
        font-size: calc( var(--chapter-font-size) + 2px );
        flex: 1 0 40%;
        font-family: var(--title-font-family);
        margin: 0.5rem;
        color: var(--title-color);
      }

      .verses__container {
        display: flex;
        flex-direction: column;
        flex-wrap: nowrap;
        margin-bottom: 5rem;
        margin-left: 1ex;
        margin-right: 1ex;
      }

      .verses__line {
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
      }

      .verse__column {
        flex: 1;
      }

      .verse__column:nth-of-type(odd) {
        margin-right: 1ch;
      }

      .chapter {
        font-size: var(--chapter-font-size);
        font-family: var(--title-font-family);
        margin: 0.5rem;
        color: var(--title-color);
      }

      .verse {
        text-align: left;
        margin: 0;
        margin-bottom: 0.3rem;
      }

      .verse__index {
        font-size: calc( var(--baiboly-verse__fontSize) - 0.5rem);

      }

      .verse__content {
        font-size: var(--baiboly-verse__fontSize);
      }

      .buttons {
        flex: 0 0 auto;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 0.25rem;
      }

      .buttons--absolute {
        position: absolute;
      }

      .buttons--relative {
        position: relative;
      }

      .font-sizer {
        font-size: 1rem;
        font-weight: 600;
        color: white;
        flex: 0 0 auto;
        line-height: 1;
        padding: 1rem;
        background-image: linear-gradient(0deg, rgba(255, 255, 255, 0.1), rgba(150, 150, 150, 0.2));
        border: solid 1px rgba(50, 50, 50, 0.1);
        box-shadow: 1px 1px 3px -2px rgba(100, 100, 100, 0.2);
        background-color: rgb(0, 77, 255);
        height: 1rem;
        width: 1rem;
        position: relative;
      }

      .font-sizer + .font-sizer {
        margin-left: 1ex;
      }

      .header {
        margin: 0;
        line-height: 1;
        flex: 0 0 auto;
        display: flex;
        padding: 0 0.75rem;
        flex-flow: row wrap;
        position: relative;
        justify-content: flex-end;
        align-items: center;
      }

      :host(.scrolled) .header::after {
        opacity: 1;
      }

      .header::after {
        content: '';
        display: block;
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        height: 10px;
        box-shadow: 0px 6px 6px rgba(0, 0, 0, 0.4);
        opacity: 0;
        transition-delay: 0s;
        transition-duration: 50ms;
        transition-property: opacity;
        transition-timing-function: ease-in;
      }

      .title {
        flex: 1 0 100%;
        color: var(--title-color);
        font-size: var(--title-font-size);
        font-weight: var(--title-font-weight);
        font-family: var(--title-font-family);
        line-height: 1;
        margin: 1rem;
      }
  `;
  }

  render() {

    const verses = this.verses.map((sequenceItem, idx) => 
      html`
        <div class="verses__line">
        ${sequenceItem.verse.map((verse, verseIndex) => html`
        <div class="verse__column">
          ${this.showChapter(idx, verse.index)
        ? html`<h3 class="chapter">${this.titleChapter(verseIndex) + ' ' + this.chapter(verse)}</h3>`
        : html``}
          <p class="verse">
            <span class="verse__index">${verse.index}</span>
            <span class="verse__content">${verse.content}</span>
          </p>
        </div>`)}
      </div>
      `
    )

    return html`
    <scroller-ko .elt="${this}" .fontSize="${this._fontSizeVerses}"></scroller-ko>
    <div class="header">
      <h3 class="title">${this.title}</h3>
      <div class="buttons buttons--absolute">
        <div style="position: relative;">
          <mwc-icon-button id="menu-trigger" label="Sokafana ny menu" @click=${() => this.menu.open = true}>${iconFormatSize}</mwc-icon-button>
          <mwc-menu id="menu">
            <mwc-list-item>
              <div class="buttons font-sizer__container">
                <div class="font-sizer font-sizer--decrease" @click=${this.decreaseFontSize}>-<mwc-ripple></mwc-ripple></div>
                <div class="font-sizer font-sizer--increase" @click=${this.increaseFontSize}>+<mwc-ripple></mwc-ripple></div>
              </div>
            </mwc-list-item>
          </mwc-menu>
        </div>
      </div>
    </div>

    <!-- <template is="dom-repeat" items="${this.verses}" index-as="idx"> -->
      <div class="book__outer">
        ${this.books.map(book => html`<h2 class="book">${book}</h2>`)}
      </div>
    <div class="verses__container">
      ${verses}
    </div>`;
  }

  constructor() {
    super();
    this.mql = window.matchMedia('screen and (max-width: 425px)');
    this.mql.addListener(this._onMediaQueryChange.bind(this));
  }

  connectedCallback() {
    super.connectedCallback();
    this._onMediaQueryChange(this.mql);
  }
  
  firstUpdated() {
    this.menu.anchor = this.shadowRoot.querySelector('#menu-trigger');
    this.menu.corner = 'BOTTOM_LEFT';
  }

  _onMediaQueryChange(evt: MediaQueryList) {
    if (evt.matches) {
      /* screen and (max-width: 425px) */
      this._fontSizeVerses = 16;
      this._fontSizeChapter = 22;
    } else {
      this._fontSizeVerses = 26;
      this._fontSizeChapter = 26;
    }
  }


  book(lang: number) {
    return this.verses[0].bookNames[lang];
  }

  decreaseFontSize(event: MouseEvent) {
    event.stopPropagation();
    this._fontSizeVerses -= 2;
  }

  chapter(verse: Andininy) {
    return verse.chapterId.split('_').slice(1)[0];
  }

  increaseFontSize(event: MouseEvent) {
    event.stopPropagation();
    this._fontSizeVerses += 2;
  }

  scrollDown() {
    this.scroller.scrollDown();
  }
  scrollUp() {
    this.scroller.scrollUp();
  }

  showBook(index: string) {
    return Number(index) === 0;
  }

  showChapter(index: number, vIndex: number) {
    return index == 0 || vIndex == 1;
  }
  titleChapter(lang: number) {
    return lang === 0 ? 'Toko' : 'Chapitre';
  }

}