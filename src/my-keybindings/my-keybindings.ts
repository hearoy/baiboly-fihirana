import type { ScrollableKo } from "src/scroller-ko/scroller-ko";
import { focusFandaharana } from '../inter-communication/intercom';

declare global {
   var MyKeybindings: {
      giveFocusToFandaharana: () => void;
      scrollDown: () => void;
      scrollUp: () => void;
      toggleVakiteny: () => void;
  }
}
/** Keybindings used to toggle overview (in reveal-index), switch to vakiteny (v),
 * scroll down (arrow down), scroll up (arrow up)
*/
var MyKeybindings = (function() {
   let previous;
   let vakiteny = '#/vakiteny';

   const giveFocusToFandaharana = focusFandaharana;

   /** 
    * from `vakiteny`, go back to saved `previous`
   */
   function goBackToPrevious() {
      vakiteny = location.hash;
      window.Reveal.setState(previous);
   }

   function goToVakiteny() {
      refreshPrevious();
      location.hash = vakiteny;
   }

   function initChildScroller() {
      const current = window.Reveal.getCurrentSlide();
      if(current.querySelector('baiboly-sequence') || current.querySelector('hr-hira')) {
         /** baiboly or hira */
         let child: ScrollableKo = current.querySelector('baiboly-sequence');
         if(child == null) {
            child = current.querySelector('hr-hira');
         }
         return { current: current, child: child };
      } else {
         throw new Error('no child scroller');
      }
   }

   function refreshPrevious() {
      previous = window.Reveal.getState();
   }

   function scrollDown() {
      try {
         const { current, child } = initChildScroller();
         child.scrollDown();
      } catch(error) {
         return;
      }

   }
   function scrollUp() {
      try {
         const { current, child } = initChildScroller();
         child.scrollUp();
      } catch(error) {
         return;
      }
   }



   function toggleVakiteny() {
      if(location.hash.includes('vakiteny')) {
         goBackToPrevious();
      } else {
         goToVakiteny();
      }
   }
   return { giveFocusToFandaharana, scrollDown, scrollUp, toggleVakiteny };

})();
(<any>window).MyKeybindings = MyKeybindings;