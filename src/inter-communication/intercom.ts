import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { filter, map } from 'rxjs/operators';

interface ChannelMessage {
   topic: 'hira' | 'baiboly'
   action: string;
   payload: string;
}
interface ChannelHiraMessage extends ChannelMessage {
   action: 'go' | 'add';
   topic: 'hira';
}
interface ChannelBaibolyMessage extends ChannelMessage {
   action: 'go';
   topic: 'baiboly';
}

const ORIGIN_FANDAHARANA = process.env.FANDAHARANA_ORIGIN;
let opener: WindowProxy;
function getOpener() {
   return opener !== window ? opener : null;
}
const channel = new ReplaySubject<ChannelMessage>();
const openerSubject = new BehaviorSubject(null);

export function hasOpener(): Observable<boolean> {
   return openerSubject.pipe(map(o => !!o));
}

function isChannelHira(channel: ChannelMessage): channel is ChannelHiraMessage {
   return channel.topic === 'hira';
}
function isChannelBaiboly(channel: ChannelMessage): channel is ChannelBaibolyMessage {
   return channel.topic === 'baiboly';
}

export function getChannelHira(): Observable<ChannelHiraMessage> {
   return channel.pipe(
      filter(isChannelHira),
   );
}
export function getChannelBaiboly(): Observable<ChannelBaibolyMessage> {
   return channel.pipe(
      filter(isChannelBaiboly),
   );
}

export const acknowledgeMessage: <M extends ChannelMessage>(message: M) => void = acknowledgeOrigin.bind(null, ORIGIN_FANDAHARANA);

function acknowledgeOrigin<M extends ChannelMessage>(origin = ORIGIN_FANDAHARANA, message: M) {
   getOpener()?.postMessage({ status: 'ok', message }, ORIGIN_FANDAHARANA);
}

export function answerWithCorrection<M extends ChannelMessage>(message: M) {
   getOpener()?.postMessage({ status: 'nok', message }, ORIGIN_FANDAHARANA);
}

function listen(origin = ORIGIN_FANDAHARANA) {
   opener = window.opener;
   openerSubject.next(opener);
   window.addEventListener('message', (event) => {
      console.log(`baiboly received ${JSON.stringify(event.data)}`);
      if (event.origin !== process.env.FANDAHARANA_URL) {
         return;
      }
      const data = event.data;
      opener = event.source as WindowProxy;
      openerSubject.next(opener);
      if (data.action === 'haody o') {
         getOpener().postMessage({ status: 'haody o' }, origin);
      }
      if (data?.action) {
         switch (data.topic) {
            case 'hira': switch (data.action) {
               case 'go':
                  channel.next({
                     topic: 'hira',
                     action: 'go',
                     payload: data.payload,
                  })
                  break;
            }
               break;
            case 'baiboly': switch (data.action) {
               case 'go':
                  channel.next({
                     topic: 'baiboly',
                     action: 'go',
                     payload: data.payload,
                  })
                  break;
            }
               break
         }
      }
   });
}

export function focusFandaharana() {
   getOpener()?.postMessage({ status: 'communicating', message: 'focus' }, ORIGIN_FANDAHARANA);
}

export function giveNoticeBeforeClosure() {
   window.addEventListener('beforeunload', (event) => {
      getOpener()?.postMessage({ status: 'closing' }, ORIGIN_FANDAHARANA);
      if (getOpener()) {
         event.preventDefault();
         event.returnValue = false;
      }
   });
}

export const listenToFandaharana: () => void = listen.bind(null, ORIGIN_FANDAHARANA);