import { MonoTypeOperatorFunction, Observable, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';

export class Tools {
  static setTimeout<T>(handler: (...args: any[]) => T, delay: number) {
    return new Promise<T>((resolve, reject) => {
      setTimeout(() => resolve(handler()), delay)
    })
  }
}

export function setTimeoutPromise<T = void>(handler?: (...args: any[]) => T, delay: number = 0) {
  return new Promise<T>((resolve, reject) => {
    setTimeout(() => resolve(handler?.()), delay);
  })
}

export const removeAccents = (s: string) => s.normalize('NFD').replace(/[\u0300-\u036f]/g, '');

export function checkForReveal() {
  return new Promise((resolve, rej) => {
     const intervalId = window.setInterval(() => {
        if(window.Reveal) {
           window.clearInterval(intervalId);
           resolve(window.Reveal);
        }
     }, 500);
  })
}

//*** Events

/**
 * 
 * Wraps addEventListener in a promise that :
 * - clears the listener on first emission
 * - or emits null when (externally) cleared without any emission
 * The first situation that happens wins over (race condition)
 */
export function fromEventOnce<K extends keyof HTMLElementEventMap>(eventName: K, elt: EventTarget, options?: EventListenerOptions & {beforeCleared?: () => void, untilDisconnected?: HTMLElement}): Promise<HTMLElementEventMap[K]>;
export function fromEventOnce(eventName: string, elt: EventTarget, options?: EventListenerOptions & {beforeCleared?: () => void, untilDisconnected?: HTMLElement}): Promise<Event>;
export function fromEventOnce(eventName: string, elt: EventTarget, options?: EventListenerOptions & {beforeCleared?: () => void, untilDisconnected?: HTMLElement}): Promise<Event> {
  return new Promise(resolve => {
    // function listener(evt: Event) {
    //   elt.removeEventListener(eventName, listener);
    //   resolve(evt);
    // }
    // elt.addEventListener(eventName, listener);
    const eventListener = addEventListenerTool(elt, eventName, (event, removeEventListener) => {
      removeEventListener();
      resolve(event);
    },
      {
        ...options,
        beforeCleared() {
          options?.beforeCleared?.();
        },
      });
    if (options?.untilDisconnected) {
      eventListener.untilDisconnected(options!.untilDisconnected);
    }
  })
}

type Methods<T> = { [K in keyof T]: T[K] extends Function ? K : never }[keyof T];

export function addEventListenerTool(target: EventTarget, event: string, handler: (event: Event, removeEventListener?: () => void) => void, opts?: AddEventListenerOptions & { beforeCleared?: () => void }) {

  function handlerMod(event: Event) {
    handler(event, removeNow);
  }

  function untilDisconnected(instance: HTMLElement) {
    let origFn = () => { };
    if (instance['disconnectedCallback']) {
      origFn = instance['disconnectedCallback'] as () => void;
    }
    Object.defineProperty(instance, 'disconnectedCallback', {
      value: function() {
        console.log('disconected')
        opts?.beforeCleared?.();
        target.removeEventListener(event, handlerMod);
        typeof origFn === 'function' && origFn.call(this);
      },
      writable: true,
    });
  }

  function removeNow() {
    opts?.beforeCleared?.();
    target.removeEventListener(event, handlerMod);
  }

  target.addEventListener(event, handlerMod, opts);
  return {
    untilDisconnected,
    removeNow,
  }
}

// *** RxJS

export function untilDisconnected<T>(instance: HTMLElement): MonoTypeOperatorFunction<T> {
  let subject: Subject<void>;
  const field = Symbol.for('disconnectedSubject');
  if (instance[field]) {
    subject = instance[field];
  } else {
    subject = new Subject();
    instance[field] = subject;
    emitOnDisconnectedCallback();
  }

  function emitOnDisconnectedCallback() {
    let origFn = () => { };
    if (instance['disconnectedCallback']) {
      origFn = instance['disconnectedCallback'] as () => void;
    }
    Object.defineProperty(instance, 'disconnectedCallback', {
      value: function() {
        console.log('disconected')
        subject.next();
        origFn.call(this);
      },
      writable: true,
    });
  }

  return takeUntil(subject);
}

export function resolveWithFirstValue<T>(obs: Observable<T>) {
  return obs.pipe(
    take(1)
  ).toPromise();
}