import '../tools/app-version';
import { setPassiveTouchGestures } from '@polymer/polymer/lib/utils/settings.js';
import '../my-keybindings/my-keybindings';
// import "fihirana-reveal/css/reveal.min.css";
import 'reveal.js/css/reveal.css'
import "fihirana-reveal/css/theme/source/fihirana01.scss";
import "../app.shell";
// import "revealJs";
import 'reveal.js/js/reveal.js';
import { giveNoticeBeforeClosure, listenToFandaharana } from '../inter-communication/intercom';
import "../style.scss";

//@ts-ignore
window.polymerSkipLoadingFontRoboto = true;
setPassiveTouchGestures(true);
    // Full list of configuration options available at:
      // https://github.com/hakimel/reveal.js#configuration
window.addEventListener('DOMContentLoaded', () => {
  Reveal.initialize({
    // Display controls in the bottom right corner
    controls: true,
    // Determines where controls appear, "edges" or "bottom-right"
    controlsLayout: 'bottom-right',
    // Display a presentation progress bar
    progress: true,
    // Push each slide change to the browser history
    history: true,
    // Vertical centering of slides
    center: false,


    // Display the page number of the current slide
    slideNumber: false,

    // Enable keyboard shortcuts for navigation
    keyboard: {
      40: MyKeybindings.scrollDown,
      37: 'prev',
      38: MyKeybindings.scrollUp,
      39: 'next',
      86: MyKeybindings.toggleVakiteny,
      27: MyKeybindings.giveFocusToFandaharana,
    },

    // Enable the slide overview mode
    overview: true,

    // Enables touch navigation on devices with touch input
    touch: false,

    // Loop the presentation
    loop: false,

    // Change the presentation direction to be RTL
    rtl: false,
    // Randomizes the order of slides each time the presentation loads
    shuffle: false,

    // Turns fragments on and off globally
    fragments: false,

    // Flags if the presentation is running in an embedded mode,
    // i.e. contained within a limited portion of the screen
    embedded: false,
    // Flags if we should show a help overlay when the questionmark
    // key is pressed
    help: true,
    // Flags if speaker notes should be visible to all viewers
    showNotes: false,

    // Global override for autolaying embedded media (video/audio/iframe)
    // - null: Media will only autoplay if data-autoplay is present
    // - true: All media will autoplay, regardless of individual setting
    // - false: No media will autoplay, regardless of individual setting
    autoPlayMedia: null,
    // Number of milliseconds between automatically proceeding to the
    // next slide, disabled when set to 0, this value can be overwritten
    // by using a data-autoslide attribute on your slides
    autoSlide: 0,
    // Stop auto-sliding after user input
    autoSlideStoppable: true,
    // Use this method for navigation when auto-sliding
    autoSlideMethod: Reveal.next,


    // Enable slide navigation via mouse wheel
    mouseWheel: false,
    // Hides the address bar on mobile devices
    hideAddressBar: true,
    // Opens links in an iframe preview overlay
    previewLinks: true,

    // Transition style
    transition: 'slide', // none/fade/slide/convex/concave/zoom

    // Transition speed
    transitionSpeed: 'default', // default/fast/slow

    // Transition style for full page slide backgrounds
    backgroundTransition: 'fade', // none/fade/slide/convex/concave/zoom

    // Number of slides away from the current that are visible
    viewDistance: 3,

    // Parallax background image
    parallaxBackgroundImage: '', // e.g. "'https://s3.amazonaws.com/hakim-static/reveal-js/reveal-parallax-1.jpg'"

    // Parallax background size
    parallaxBackgroundSize: '', // CSS syntax, e.g. "2100px 900px"
    // Number of pixels to move the parallax background per slide
    // - Calculated automatically unless specified
    // - Set to 0 to disable movement along an axis
    parallaxBackgroundHorizontal: null,
    parallaxBackgroundVertical: null,

    // The "normal" size of the presentation, aspect ratio will be preserved
    // when the presentation is scaled to fit different resolutions. Can be
    // specified using percentage units.
    width: '100%', //960,
    height: '100%', //700,

    // Factor of the display size that should remain empty around the content
    margin: 0,

    // Bounds for smallest/largest possible scale to apply to content
    minScale: 0.2,
    maxScale: 1.5,
    // The display mode that will be used to show slides
    display: 'flex',

    // Optional reveal.js plugins
    dependencies: [
      // Cross-browser shim that fully implements classList - https://github.com/eligrey/classList.js/
      // {
      // 	src: 'lib/js/classList.js',
      // 	condition: function () {
      // 		return !document.body.classList;
      // 	}
      // },
      // Interpret Markdown in <section> elements
      // {
      //    src: 'plugin/markdown/marked.js',
      //    condition: function () {
      //       return !!document.querySelector('[data-markdown]');
      //    }
      // },
      // {
      //    src: 'plugin/markdown/markdown.js',
      //    condition: function () {
      //       return !!document.querySelector('[data-markdown]');
      //    }
      // },
      // Syntax highlight for <code> elements
      //{ src: 'plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
      // Zoom in and out with Alt+click
      //{ src: 'plugin/zoom-js/zoom.js', async: true },
      // Speaker notes
      // { 

      // Remote control your reveal.js presentation using a touch device
      //               { src: 'plugin/remotes/remotes.js', async: true },

      // MathJax
      //               { src: 'plugin/math/math.js', async: true }
    ]
  });
  Reveal.addEventListener('ready', (event) => {
    document.querySelector(".slides.noFoucPls").classList.remove("noFoucPls");
    document.querySelector("fanipazana-shell.noFoucBckg").classList.remove("noFoucBckg");
    const critPath = document.querySelector('#criticalPath');
    const cPParent = critPath.parentElement;
    cPParent.removeChild(critPath);
    listenToFandaharana();
    giveNoticeBeforeClosure();
  });
})
       // Reveal.addEventListener('ready', function(event) {
         // removeFoucPreventer();