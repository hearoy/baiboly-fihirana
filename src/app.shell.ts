import {
  html,
  customElement,
  internalProperty,
  query,
  LitElement,
  property,
} from 'lit-element';
import { repeat as htmlRepeat } from 'lit-html/directives/repeat';
import { nothing } from 'lit-html';
import { fromEventOnce } from './tools';
import { queueMusic, transit_enterexit } from './tools/my-icons';
import { hasOpener, focusFandaharana } from './inter-communication/intercom';
import '../fihirana-reveal/lib/component/hira-manager/hira-keyboard/hira-keyboard';
import type { HiraWithHighlightsAndIndex } from 'fihirana-reveal/lib/component/hira-manager/hira-manager';

declare global {
  interface HTMLElementTagNameMap {
    'fanipazana-shell': FanipazanaShell;
  }
}

@customElement('fanipazana-shell')
class FanipazanaShell extends LitElement {
  @internalProperty()
  private opened: boolean = false;
  @internalProperty()
  private sequences: any[] = [];
  @internalProperty()
  private hiraBundleList: HiraWithHighlightsAndIndex[] = [];
  @property()
  documentTitle = 'Baiboly sy fihirana';

  @query('recherche-hira')
  rechercheHira: any;// RechercheHira;
  private showFocusFandaharana: boolean;


  render() {
    return html`
      <style>
      #drawer {
        position: absolute;
        top: 0;
        width: 100%;
      }
      .accueil h2.title {
        flex: 1 0 100%;
      }
      .drawer__overlay {
        background: red;
      }
      </style>
        <!-- Any section element inside of this container is displayed as a slide -->
        
        <div class="slides noFoucPls">
          <section class="accueil present" id="accueil-logo">
            <picture> 
              <source sizes="90vw"
                      type="image/webp"
                      srcset="res/enTeteAfficheFPMAOCentreRoundSketchTraits--2160w.webp 2160w,
                              res/enTeteAfficheFPMAOCentreRoundSketchTraits--1440w.webp 1440w,
                              res/enTeteAfficheFPMAOCentreRoundSketchTraits--720w.webp   720w">
              <img src="res/enTeteAfficheFPMAOCentreRoundSketchTraits--1080w.jpg"
                   alt="FPMA Orléans"
                   style="max-height:20%; width:90vw; object-fit:contain;"
                   sizes="90vw"
                   class="home-img"
                   srcset="res/enTeteAfficheFPMAOCentreRoundSketchTraits--2160w.jpg 2160w,
                           res/enTeteAfficheFPMAOCentreRoundSketchTraits--1440w.jpg 1440w,
                           res/enTeteAfficheFPMAOCentreRoundSketchTraits--720w.jpg   720w"/>
            </picture>

            <h2 class="title">${this.documentTitle}</h2>

            <div id="logo-fpmao" class="img">
              <svg>
                <use xlink:href="#logosvg" />
              </svg>
            </div>

            <div id="zipLink">
              <p contenteditable="true"></p>
            </div>
          </section>
          <section class="recherche future" id="mitady-tononkira">
            <h2 class="section__title">Fitadiavana tononkira</h2>
            <recherche-motif></recherche-motif>
          </section>

          <section id="mifidy-vakiteny">
            <h2 class="section__title">Mifidy vakiteny</h2>
            <baiboly-dispatcher @sequenceschanged=${this.sequencesChanged}></baiboly-dispatcher>
          </section>
          ${this.sequences?.length > 0
        ? html`
                <section id="vakiteny" class="stack future">
                  ${this.sequences.map(
          (item, index) => html`
                      <section id=${'vakiteny-' + index} class="future">
                        <baiboly-sequence .verses=${item}></baiboly-sequence>
                      </section>
                    `
        )}
                </section>
              `
        : nothing}

          <section id="mifidy-hira" class="future">
            <h2 class="section__title flex-1 vertical-center">
              Mifidy ny hira
            </h2>
            <hira-manager id="mpitarika-hira"
                          .hiraList=${this.hiraBundleList}
                          @hira-list-change=${this.hiraListChanged}></hira-manager>
          </section>
          ${htmlRepeat(this.hiraBundleList, (item) => item.hira.id,
          (item) => html`
                  <section class="future auto-added-hira">
                    <hr-hira .hira=${item.hira}
                            .index=${item.index}
                            .highlights=${item.andininy}></hr-hira>
                  </section>
                `
        )
        }
        </div>
        <aside class="custom-buttons">
          <mwc-icon class="custom-buttons__icon recherche-button"
                    @click=${this.openRecherche}>${queueMusic}</mwc-icon>
          ${this.showFocusFandaharana
              ? html`<mwc-icon-button class="custom-buttons__icon custom-buttons__icon--focus-fandaharana"
                                      @click=${this.focusFandaharana}>${transit_enterexit}</mwc-icon-button>`
              : nothing}
        </aside>
        <recherche-hira id="rechercheHira" @hira-list-change=${this.hiraListChanged}></recherche-hira>
        <swipe-reveal></swipe-reveal>
        <mwc-drawer id="drawer"
                    .open=${this.opened}
                    type="modal"
                    @MDCDrawer:opened=${this.drawerOpened}
                    @MDCDrawer:closed=${this.drawerClosed}>
        <reveal-index .displayed=${this.opened}
                      @overview-clicked=${() => this.opened = false}
                      @slide-clicked=${() => this.opened = false}></reveal-index>
        <div class="drawer__overlay" slot="appContent"></div>
      </mwc-drawer>
        <aside class="logo-menu">
          <svg @click=${this.openDrawer} id="logo-menu">
            <use xlink:href="#logosvg" />
          </svg>
          <mwc-ripple></mwc-ripple>
        </aside>
        <div id="reveal__after">
          <svg>
            <use xlink:href="#logosvg" />
          </svg>
        </div>
    `;
  }

  constructor() {
    super();
    (async function injectThemes(paths: [path: Promise<{default: string}>, mediaQuery: string][]) {
      for (const [promisePath, mq] of paths) {
        const style = document.createElement('style');
        // style.rel = 'stylesheet';
        style.media = mq;
        ({default: style.innerHTML} = await promisePath);
        document.head.appendChild(style);
      }
    })([
      [import('./theme/style-light.theme.scss'), '(prefers-color-scheme: light)'],
      [import('./theme/style-dark.theme.scss'), '(prefers-color-scheme: dark)']
    ]);
  }
  
  connectedCallback() {
    super.connectedCallback();
    fromEventOnce('sections-added', document).then(() => {
      this.documentTitle = document.title;
    });
    import('./lazy-deps')
      .then(() => this.requestUpdate());

    // FIXME unsubscribe
    hasOpener().subscribe(hasOpener => {
      this.showFocusFandaharana = hasOpener;
      if (hasOpener) {
        this.requestUpdate();
      }
    });
  }

  protected createRenderRoot() {
    return this;
  }

  private drawerClosed(event: CustomEvent): void {
    this.opened = false;
  }
  private drawerOpened(event: CustomEvent): void {
    this.opened = true;
  }

  firstUpdated() { }

  focusFandaharana() {
    focusFandaharana();
  }

  /**
   * @callback HiraListChangedEvent
   * @param {CustomEvent<{value: {indexSplices: {object: any[]}[]}; path: string>} event
   * @returns {void}
   */
  /** @type {HiraListChangedEvent} */
  hiraListChanged(event) {
    this.hiraBundleList = event.detail.hiraList.slice();
  }

  openDrawer(e) {
    this.opened = true;
  }

  /** used to open hira-manager in a popover,
   * used in aside.custom-buttons
   */
  openRecherche() {
    this.rechercheHira.open();
  }

  sequencesChanged(event) {
    this.sequences = event.detail.newValue.slice();
    setTimeout(() => Reveal.sync());
  }
}
