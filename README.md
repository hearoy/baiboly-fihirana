# \<baiboly_reveal\>

Baiboly teny gasy, reveal.js

## Install the dependencies

First, make sure you have [yarn](https://yarnpkg.com/lang/en/docs/install/) or [npm](https://nodejs.org/) installed. Then run `yarn install` to install dependencies.

## Viewing Your Application
### server for live development
```
$ yarn serve
```

## Building Your Application

```
$ yarn build
```

This will create builds of your application in the `mixDist/` directory, optimized to be served in production. You can then serve the built versions.    
