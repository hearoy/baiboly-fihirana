import type  * as webpack from 'webpack';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
import * as CopyWebpackPlugin from 'copy-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import * as ExtractCssChunks from 'extract-css-chunks-webpack-plugin';
import * as OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin';
import * as CompressionPlugin from 'compression-webpack-plugin';
import * as WorkboxPlugin from 'workbox-webpack-plugin';
import * as TerserPlugin from 'terser-webpack-plugin';
import * as HtmlWebpackTagsPlugin from 'html-webpack-tags-plugin';
import { TsconfigPathsPlugin } from 'tsconfig-paths-webpack-plugin';
import * as ScriptExtHtmlWebpackPlugin from 'script-ext-html-webpack-plugin';
import * as path from 'path';
import * as rxjsPathMap from 'rxjs/_esm2015/path-mapping';
import * as DotEnv from 'dotenv-webpack';
import type { TerserPluginOptions } from 'terser-webpack-plugin';


type CompressOptions = Exclude<TerserPluginOptions['terserOptions']['compress'], boolean>

let MIX = {
  outputDir: path.resolve(__dirname, 'mixDist'),
  index: path.resolve(__dirname, 'index.mix.ejs'),
};
const PATHS = {
  entry: {
    webcomponentsLoader: path.resolve(__dirname, 'node_modules/@webcomponents/webcomponentsjs/webcomponents-loader.js'),
    app: path.resolve(
      __dirname,
      'src/baiboly_reveal-app/baiboly_reveal-app.ts'
    ),
  } as webpack.Entry,
};

const FIHIRANA_PATHS = {
  base: path.resolve(__dirname, 'fihirana-reveal'),
};

const revealCopy = [
  {
    from: 'css/print/*.css',
    to: 'css/print/[name].[ext]',
  },
  {
    from: 'res/*',
    to: 'res/[name].[ext]',
  },
];

const revealMixCopy = revealCopy.map(ft => {
  return { from: path.resolve(FIHIRANA_PATHS.base, ft.from), to: ft.to };
});

/**                               **/
/** FIHIRANA sy Baiboly configuration  */
/**                               **/

const configMix: (mode: 'production' | 'development') => webpack.Configuration = (mode: 'production' | 'development') => ({
  // Tell Webpack which file kicks off our app.
  entry: PATHS.entry, //mix(FIHIRANA_PATHS.entry,PATHS.entry),
  // Tell Weback to output our bundle to ./dist/bundle.js
  output: {
    filename: '[name].[contenthash].js',
    path: MIX.outputDir,
    publicPath: '',
    // chunkFilename: '[name].lazy.js'
    // publicPath: 'https://fanipazana.fpmaorleans.fr'
  },
  mode,
  devServer: {
    contentBase: MIX.outputDir,
    compress: true,
    https: false,
    open: false,
    overlay: true,
    port: 9000,
    before: function(app) {
      app.use((req, res, next) => {
        if (req.header('purpose')?.match(/prefetch/i)?.length || req.path.match(/\.json$/)?.length) {
          res.setHeader('Cache-Control', 'public, max-age=300');
          next();
        } else {
          next();
        }
      })
    }
  },
  // Tell Webpack which directories to look in to resolve import statements.
  // Normally Webpack will look in node_modules by default but since we’re overriding
  // the property we’ll need to tell it to look there in addition to the
  // bower_components folder.
  resolve: {
    extensions: ['.ts', '.js', '.css', 'scss'],
    modules: ['node_modules'],
    alias: rxjsPathMap(),
    // alias: {
    //   revealJs: path.resolve(__dirname, 'fihirana-reveal/js/reveal.min.js'),
    //   baiboly: path.resolve(__dirname)
    // },
    plugins: [new TsconfigPathsPlugin({configFile: 'tsconfig.app.json'})],
  },
  // resolveLoader: {
  //   modules: [path.resolve(__dirname), 'node_modules'],
  // },
  // These rules tell Webpack how to process different module types.
  // Remember, *everything* is a module in Webpack. That includes
  // CSS, and (thanks to our loader) HTML.
  module: {
    // noParse: [/\.json$/, /webcomponentsjs/],
    rules: [
      // {
      //   test: /fihirana\.json$/,
      //   // exclude: /fihirana\.json$/,
      //   type: 'javascript/auto',
      //   use: [
      //     'raw-loader',
      //     'file-loader',
      //   ]
      // }, 
      // {
      //   // If you see a file that ends in .html, send it to these loaders.
      //   test: /\.html$/,
      //   // This is an example of chained loaders in Webpack.
      //   // Chained loaders run last to first. So it will run
      //   // polymer-webpack-loader, and hand the output to
      //   // babel-loader. This let's us transpile JS in our `<script>` elements.
      //   use: [

      //     // { loader: 'style-loader/url', options: { insertAt: 'top' } },
      //     // { loader: 'css-loader', options: { minimize: true } },
      //     // { loader: 'file-loader' },

      //     {
      //       loader: 'babel-loader',
      //       options: {
      //         presets: [[
      //           '@babel/preset-env',
      //           {
      //             targets: 'last 2 Chrome versions'
      //           }
      //         ]]
      //         // presets: [['@babel/preset-env', {
      //         //   exclude: ["@babel/plugin-transform-modules-commonjs"]
      //         // }
      //         // ]]
      //       }
      //     },

      //     // {loader: 'ts-loader'},
      //     // {
      //     //   loader: 'polymer-webpack-loader',
      //     //   options: {
      //     //     processStyleLinks: true,
      //     //     htmlLoader: {
      //     //       attrs: ['link:href']
      //     //     }
      //     //   }
      //     // }
      //   ]
      // },
      {
        test: {
          or: [/reveal\.js$/, /revealJs/],
        },
        use: [
          {
            loader: 'script-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
      // {
      //   // If you see a file that ends in .js, just send it to the babel-loader.
      //   test: /\.js$/,
      //   use: [
      //     // {
      //     //   loader: 'babel-loader',
      //     //   options: {
      //     //     presets: [
      //     //       ['@babel/preset-env', { targets: 'last 2 Chrome versions' }]
      //     //     ],
      //     //     // plugins: ['@babel/plugin-syntax-dynamic-import']
      //     //   }
      //     // }
      //   ],
      //   // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
      //   // exlude web-animations-js/* because it doesn't support "use strict" applied by babel
      //   exclude: [
      //     /node_modules\/(?!polymer-webpack-loader\/).*/,
      //     /bower_components\/web-animations-js\/.*/,
      //     /bower_components\/webcomponentsjs/,
      //     /node_modules\/@webcomponents\/webcomponentsjs\/.*/,
      //     /reveal\.min\.js/
      //   ],
      // },
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: 'last 2 Chrome versions',
                  },
                ],
              ],
            },
          },
          {
            loader: 'ts-loader',
            options: {
              configFile: 'tsconfig.app.json',
            }
          },
        ],
        // Optionally exclude node_modules from transpilation except for polymer-webpack-loader:
        // exclude: /node_modules\/(?!polymer-webpack-loader\/).*/
      },
      {
        test: /(web-animations.*\.min|polymer-decorators)\.js$/,
        use: 'imports-loader?this=>window',
      },
      {
        test: /\.css$/,
        use: [
          // {
          //   loader: 'file-loader',
          //   options: {
          //     useRelativePath: true,
          //     name: '[name].[ext]'
          //   }
          // },
          mode !== 'production'
            ? 'style-loader' :
          {
            loader: ExtractCssChunks.loader,
            options: { esModule: true }
          },
          // {loader: 'to-string-loader'},
          // {
          //     loader: 'css-loader',
          //     options: {
          //       url: true,
          //       import: true,
          //       importLoaders: 2
          //     }
          //   },
          // { loader: 'extract-loader' },
          // {
          //   loader: 'resolve-url-loader'
          // }
          {
            loader: 'css-loader',
            options: { sourceMap: true, url: true, import: true }, // `esModule: true` not supported by ExtractCssChunks
          },
        ],
        // { loader: 'style-loader/url' },
        // { loader: 'extract-loader' },
        //   ExtractTextPlugin.extract({
        //     fallback: 'style-loader',
        //     use: [
        //       {
        //         loader: 'css-loader',
        //         options: {

        //         }
        //       },
        //       {
        //         loader: 'resolve-url-loader'
        //       }
        //     ]

        // })
      },
      {
        test: /theme\.scss$/,
        use: [
          'css-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.scss$/,
        exclude: /theme\.scss$/,
        use: [
          mode !== 'production'
            ? 'style-loader' :
          {
            loader: ExtractCssChunks.loader,
            options: { esModule: true }
          },
          {
            loader: 'css-loader',
            options: { sourceMap: true, url: true, import: true }, // `esModule: true` not supported by ExtractCssChunks
          },
          {
            loader: 'resolve-url-loader',
            options: {
              keepQuery: true,
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true, // needed for resolve-url-loader
            }
          }
        ],
      },
      {
        test: /\.(ttf|eot|woff2?|svg)(\?.*)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              useRelativePath: true,
              name: '[path][name].[ext]',
              esModule: true,
            },
          },
        ],
      },
    ],
  },
  optimization: {
    moduleIds: 'hashed',
    concatenateModules: true,
    providedExports: true,
    usedExports: true,
    mergeDuplicateChunks: true,
    minimize: true,
    runtimeChunk: 'single',
    splitChunks: {
      // minChunks: 2
      // chunks: (chunk) => {
      //   return chunk.canBeInitial() && !chunk.name.match('webcomponentsLoader'); // do not split chunks that should be inlined
      // },
      cacheGroups: {
        litElement: {
          test: (module: webpack.compilation.Module, chunks: webpack.compilation.Chunk[]) => {
            return !!module.context.match(/\/node_modules\/lit-(element|html)\/?/);
          },
          chunks: 'all',
          minChunks: 1,
          priority: -10,
        },
        vendors: false,
        default: false,
        lazyDeps: {
          chunks: 'async',//(chunks) => !chunks.canBeInitial(),
          enforce: true,
          minChunks: 1,
          reuseExistingChunk: true,
          priority: -20,
        },
      }
    },
  },
  plugins: [
    // new ExtractTextPlugin({
    //   filename: 'css/[name].css',
    //   allChunks: true
    // }),

    new CleanWebpackPlugin(),

    // This plugin will copy files over for us without transforming them.
    // That's important because the custom-elements-es5-adapter.js MUST
    // remain in ES2015.
    new CopyWebpackPlugin({
      patterns: revealMixCopy.concat([
        // {
        //   from: path.resolve(__dirname, 'src/data/*.json'),
        //   to: 'data/[name].[ext]'
        //   },
        // {
        //   from: path.resolve(FIHIRANA_PATHS.base, 'subsites/'),
        //   to: './[path][name].[ext]',
        //   toType: 'template',
        // },
        {
          from: 'manifest.json',
          to: './',
        },
        {
          from: path.resolve(FIHIRANA_PATHS.base, 'res/icons/*.png'),
          to: 'res/icons',
          //@ts-ignore
          flatten: true,
        },
        {
          from: 'node_modules/@webcomponents/webcomponentsjs/*.js',
          to: 'node_modules/@webcomponents/webcomponentsjs/[name].[ext]',
        },
        {
          from: 'src/data/{baiboly,bibleFr,bokyRefs}.json',
          to: 'data/[name].[ext]',
        },
        {
          from: 'fihirana-reveal/fihirana.json',
          to: 'data/[name].[ext]',
        },
      ]),
    }),
    // This plugin will generate an index.html file for us that can be used
    // by the Webpack dev server. We can give it a template file (written in EJS)
    // and it will handle injecting our bundle for us.
    new HtmlWebpackPlugin({
      // excludeChunks: ['webpack', 'common'],
      // chunksSortMode: function (chunk1, chunk2) {
      //   var orders = ['rxjs', 'polymer', 'app'];
      //   var order1 = orders.indexOf(chunk1.names[0]);
      //   var order2 = orders.indexOf(chunk2.names[0]);
      //   if (order1 > order2) {
      //     return 1;
      //   } else if (order1 < order2) {
      //     return -1;
      //   } else {
      //     return 0;
      //   }
      // },
      // chunkOrder: ['vendor','fihiranaShell','app'],
      templateParameters: (compilation, assets, assetTags, options) => {
        const scriptTags = assetTags.bodyTags.filter(({ tagName }) => tagName === 'script');
        const styleTags = assetTags.headTags.filter(({ tagName }) => tagName === 'link');
        /** Add script entry points that should have their source inlined inside the array */
        // const inlinedJs = ['webcomponentsLoader'].reduce<Set<string>>((set, name) => {
        const inlinedJs = [].reduce<Set<string>>((set, name) => {
          const idx = assets.js.findIndex((a) => a.match(name));
          assetTags.bodyTags.splice(scriptTags.findIndex(tag => (tag.attributes?.src as string)?.match(name)));
          return idx > -1 ? set.add(assets.js.splice(idx)[0]) : set;
        }, new Set());
        /** Add style entry points that should have their source inlined  inside the array */
        const inlinedCss = [].reduce<Set<string>>((set, name) => {
          const idx = assets.css.findIndex((a) => a.match(name));
          assetTags.headTags.splice(styleTags.findIndex(tag => (tag.attributes.href as string).match(name)));
          return idx > -1 ? set.add(assets.css.splice(idx)[0]) : set;
        }, new Set());

        function getSourceInSet(set: Set<string>)  {
          return function getSourceFromPattern(pattern: string | RegExp): string {
            let needle: RegExp;
            if (typeof pattern === 'string') {
              needle = new RegExp(pattern);
            } else {
              needle = pattern;
            }
            for (const name of set.values()) {
              if (name.match(needle)?.length) {
                const compilationAsset = name.substr(assets.publicPath?.length ?? 0);
                return compilation.assets[compilationAsset].source() + '\n\n';
              }
            }
          }
        }
        const inline = {
          js: getSourceInSet(inlinedJs),
          css: getSourceInSet(inlinedCss)
        };
        return {
          compilation,
          webpackConfig: compilation.options,
          htmlWebpackPlugin: {
            tags: assetTags,
            files: assets,
            options
          },
          mode,
          inline,
        }; 
      },
      debugGA: false,
      template: MIX.index,
      inject: false,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new ScriptExtHtmlWebpackPlugin({
      inline: [/runtime/, /webcomponentsLoader/],
      // prefetch: ['bokyRefs', 'bibleFr', 'baibolyMg'],
      preload: [/litElement/]
    }),
    new HtmlWebpackTagsPlugin({
      links: [
        { path: '/data', glob: '{bokyRefs,bibleFr,baiboly,fihirana}.json', globPath: 'src/data', attributes: { rel: 'prefetch', as: 'fetch', crossorigin: '' } },
        { path: '/res/fonts/open-sans/fonts/Semibold/OpenSans-Semibold.woff2', attributes: {rel: 'preload', as: 'font', crossorigin: '', type: 'font/woff2'}},
        { path: '/res/fonts/open-sans/fonts/SemiboldItalic/OpenSans-SemiboldItalic.woff2', attributes: {rel: 'preload', as: 'font', crossorigin: '', type: 'font/woff2'}},
        { path: '/res/fonts/open-sans/fonts/Bold/OpenSans-Bold.woff2', attributes: {rel: 'preload', as: 'font', crossorigin: '', type: 'font/woff2'}},
        { path: '/res/fonts/open-sans/fonts/Regular/OpenSans-Regular.woff2', attributes: {rel: 'preload', as: 'font', crossorigin: '', type: 'font/woff2'}},
        { path: '/res/fonts/playfair-display/PlayfairDisplay-Bold.woff2', attributes: {rel: 'preload', as: 'font', crossorigin: '', type: 'font/woff2'}},
      ],
      append: false,
    }),
    new ExtractCssChunks({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: '[name].css',
      chunkFilename: '[id].css',
      ignoreOrder: true, // Disable to remove warnings about conflicting order between imports,
    }),
  ],
});
export default (env: any) => {
  /** config workbox for production
   * because server mode throws a glob error
   */
  const workboxPlugin = new WorkboxPlugin.GenerateSW({
    // globDirectory: 'mixDist',
    // globPatterns: [
    //   '**/PlayfairDisplay-Bold.woff2*',
    //   '**/OpenSans-Semibold.woff2*',
    //   '**/OpenSans-Bold.woff2*',
    //   '**/OpenSans-SemiboldItalic.woff2*',
    // ],
    navigateFallback: '/',
    exclude: [
      'runtime.js',
      'webcomponentsLoader.js',
      /logo1\.5/,
      /faviconEtd/,
      /enTeteAfficheFPMAOCentre0?_old.jpg$/,
      /enTeteAfficheFPMAOCentre0.jpg$/,
      /fonts.*\.svg$/,
      /.*PlayfairDisplaySC.*/,
      /PlayfairDisplay-(?!Bold)/,
      /PlayfairDisplay-BoldItalic/,
      /\.(eot|ttf)/,
      /\/open-sans\/fonts\/(Light*|Italic|BoldItalic|ExtraBold)/,
      'vendors~app.js.LICENSE.txt',
    ],
    dontCacheBustUrlsMatching: /\w{20}\..{,6}$/,
    swDest: 'sw.js',
    maximumFileSizeToCacheInBytes: 6350000,
    offlineGoogleAnalytics: false,
    clientsClaim: true,
    skipWaiting: true,
    directoryIndex: 'index.html',
    ignoreUrlParametersMatching: [/./],
    runtimeCaching: [
      {
        urlPattern: new RegExp(
          'https://.*.fpmaorleans.fr/.*\\.(?:css|js|json|html)'
        ),
        handler: 'staleWhileRevalidate',
        options: {
          cacheName: 'staticRessources',
          broadcastUpdate: {channelName: 'sw:fanipazana-updated'},
        },
      },
      {
        urlPattern: new RegExp('https://.*.fpmaorleans.fr/.*\\.(?:jpg|png)'),
        handler: 'cacheFirst',
        options: {
          cacheName: 'images',
        },
      },
      // {
      //   urlPattern: new RegExp(
      //     'https://localhost:\\d+/.*\\.(?:css|js|json|html|jpg|png)'
      //   ),
      //   handler: 'staleWhileRevalidate',
      // },
      {
        urlPattern: new RegExp(
          '^https://fonts.(?:gstatic|googleapis).com/(.*)'
        ),
        handler: 'cacheFirst',
        options: {
          cacheName: 'googleApis',
          expiration: {
            maxEntries: 30,
          },
        },
      },
    ],
  });

  const terserPluginOpts = {
    parallel: 2,
    terserOptions: {
      ie8: false,
      ecma: 7,
      sourceMap: true,
    },
  } as TerserPlugin.TerserPluginOptions;

  let config: webpack.Configuration;
  
  const bundleAnalyzerOpts = {
    analyzerMode: 'static',
  } as BundleAnalyzerPlugin.Options;
  if (env?.serve) {
    bundleAnalyzerOpts.openAnalyzer = false;
    config = configMix('development');
    config.plugins.push(new DotEnv({ path: './src/env/dev.env' }));
    config.mode = 'development';
  /** no workbox when watching and serving */
    config.devtool = 'cheap-module-eval-source-map';
    const compressOpts = terserPluginOpts.terserOptions.compress as CompressOptions;
    terserPluginOpts.terserOptions.compress = {...compressOpts, drop_debugger: false, drop_console: false } as CompressOptions
    config.optimization.minimizer = [new TerserPlugin(terserPluginOpts), new OptimizeCSSAssetsPlugin({})];
    if (env.remote) {
      config.devServer.disableHostCheck = true;
    }
  } else {
    config = configMix('production');
    config.optimization.minimizer = [new TerserPlugin(terserPluginOpts), new OptimizeCSSAssetsPlugin({})];
    config.plugins.push(workboxPlugin);
    config.plugins.push(new CompressionPlugin({
      test: /\.(css|js|html|json)/,
    }));
  }
  config.plugins.push(new BundleAnalyzerPlugin(bundleAnalyzerOpts));

  if (env?.gitlab) {
    config.plugins.push(new DotEnv({ path: './src/env/prod.env',  }));
    config.output.publicPath = 'https://fanipazana.fpmaorleans.fr/';
  }

  return config;
};
