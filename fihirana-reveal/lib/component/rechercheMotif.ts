
import { PolymerElement, html } from "@polymer/polymer";
import { customElement, property, query } from "@polymer/decorators";
import { PaperInputElement } from "@polymer/paper-input/paper-input";
import { IronInputElement } from "@polymer/iron-input/iron-input";
import { PaperButtonElement } from "@polymer/paper-button/paper-button";
import "@polymer/paper-styles/element-styles/paper-material-styles";
import "@polymer/paper-item/paper-item";
import "@polymer/paper-item/paper-item-body";
import "@polymer/iron-icon/iron-icon";
import "../../../res/icons/myicons";
import "@polymer/iron-icon/iron-icon";
import "@polymer/polymer/lib/elements/dom-if";
import "@polymer/polymer/lib/elements/dom-repeat";
import "@polymer/paper-styles/shadow";
import "@webcomponents/shadycss/entrypoints/apply-shim.js";
import "@polymer/paper-spinner/paper-spinner";
import { RechercheHira } from "./recherche";
import { Fihirana } from './hr-hira/hr-hira';
import { TemplateInstanceBase } from '@polymer/polymer/lib/utils/templatize';
import { fromEventOnce, removeAccents } from 'src/tools';
import { getFihirana } from 'src/fihirana/fihirana-puller/fihirana-puller';

declare global {
  interface Window {
    Reveal: any;
  }
}

@customElement('recherche-motif')
class RechercheMotif extends PolymerElement {

  static get template() {
    return html`
      
    <style is="custom-style" include="paper-material-styles">
     :host paper-button {
      transition: background-color 0.3s ease;
    }

    @media screen and (min-width:600px) {
      .outer {
        max-width: 600px;
      }
    }
     :host {
      flex: 1;
      max-height: 100%;
      width: 100%;
      display: flex;
      flex-flow: column nowrap;
      align-items: center;
      justify-content: flex-start;
      box-sizing: border-box;

      --paper-button: {
        margin: 1ex;
        text-transform: none;
      };

      --paper-button-disabled: {
        background: #eaeaea;
        color: #a8a8a8;
      }
      --paper-input-container-color: var(--fpmao-input-color);
    }

     :host paper-button:hover {
      background-color: rgba(38, 73, 147, .2);
    }

    .enRelief {
      --paper-button: {
        color: #fff;
        background-color: rgba(38, 73, 147, .8);
        text-transform: none;
      }
      ;
    }

     :host paper-button.enRelief:hover {
      background-color: #264993;
    }

     :host .paper-material,
     :host .paper-material::shadow * {
      max-width: 700px;
      /* margin: auto; */
      --paper-material_-_display: block;
    }

     :host .paper-material[elevation="1"] {
       @apply --paper-material-elevation-1;
    }

    .inputs-container {
      margin: 1ex;
      overflow: auto;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-flow: row wrap;
    }

    .input-outer {
      flex: 1 0 100%;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-flow: row wrap;
    }

    #input {
      text-align: left;
      max-width: 15em;
      flex: 1;
    }

    .resultats__item {
      margin-bottom: 1rem;
    }
    [secondary]{
      white-space: pre-line;
    }

    paper-spinner {
      align-self: center;
    }
    

  </style>
  


    <div class="outer">
      <div class="paper-material" elevation="1">
        <div class="inputs-container">
          <div class="input-outer">
            <paper-input id="input" value="{{inputValue}}" label="Teny tadiavina"></paper-input>
          </div>
          <paper-button on-tap="clearInput">Fafàna</paper-button>
          <paper-button class="enRelief" id="tadiavinaButton" disabled="[[disabled]]" on-tap="tadiavo">
            <iron-icon icon="myicons:find-in-page"></iron-icon>
            [[_tadiavina]]</paper-button>
        </div>
  
      </div>
      <paper-spinner active="[[_waiting]]" style$="[[_spinnerStyle]]"></paper-spinner>
      <template is="dom-if" if="{{_noResults}}">Tsy hita o!</template>
      <template class="dom-if-results" is="dom-if" if="{{_displayResults}}">
        <template id="dom-repeat" is="dom-repeat" items="{{occurencesListe}}">
          <div class="paper-material resultats__item" elevation="1">
            <paper-item on-click="goToHira">
              <paper-item-body two-line>
                <div>{{item.hira}}</div>
                <div secondary inner-h-t-m-l="[[item.str]]"></div>
              </paper-item-body>
            </paper-item>
          </div>
        </template>
      </template>
    </div>
      `;
  }

  @property({ type: String })
  inputValue: string;

  @property({ type: Boolean, computed: '_inputIsEmpty(inputValue)' })
  disabled: boolean;

  @property({ type: String })
  filepathToSearchIn: string;

  @property({ type: String, computed: '_tadiavinaLabel(inputValue)' })
  _tadiavina = 'Tadiavina';

  @property({ type: Boolean })
  _displayResults: boolean;
  @property({ type: Boolean })
  _noResults: boolean;

  @property({ type: String, computed: '_spinnerStyler(_waiting)' })
  _spinnerStyle: string;

  @query('#tadiavinaButton')
  tadiavinaButton: PaperButtonElement;

  importe: Document;

  @query('#input') input: PaperInputElement;
  occurencesListe: Occurence[];
  alwaysFalse: () => false;
  _waiting: boolean;

  _inputIsEmpty(inputValue) {
    return inputValue ? false : true;
  }
  _spinnerStyler(_waiting) {
    return _waiting ? 'display:block' : 'display:none';
  }
  _tadiavinaLabel(inputValue) {
    var str = inputValue ? ': \"' + inputValue + '\"' : '';
    return 'Tadiavina ' + str;
  }
  get pattern() {
    // var accents = /[\u00c0-\u00ff]/;
    // \b is not recognized around an accented character
    // if (accents.test(this.inputValue[0]))
    //   prefixe = '.*';
    // else
    const prefixe = '.*\\b';

    const suffixe = '\\b.*';

    const normalizedInput = removeAccents(this.inputValue.replace(/ +/g, String.raw`\W+`));
    var patt = String.raw`${prefixe}(${normalizedInput})${suffixe}`;
    return new RegExp(patt, 'im');
  }

  get inputElt() {
    return this.input.inputElement as IronInputElement;
  }

  ready() {
    super.ready();
    this.occurencesListe = [];
    // to keep reveal.js from acting on keypresses
    this.alwaysFalse = () => false;
    this._waiting = false;
    this.input.addEventListener('focus', this._onFocus.bind(this));
    this.input.addEventListener('blur', this._onBlur.bind(this));
    this.input.addEventListener('keydown', (e) => this._onKeydown(e));
  }

  _onFocus(e: FocusEvent) {
    const reveal = window.Reveal || '';
    if (reveal) // keep reveal.js from acting on keypresses
      reveal.configure({
        keyboardCondition: this.alwaysFalse
      });
  }
  _onKeydown(e: KeyboardEvent) {
    //console.log(e.keyCode);
    if (e.keyCode === 13)
      this.tadiavinaButton.click();
  }
  _onBlur() {
    var reveal = window.Reveal || '';
    if (reveal)
      reveal.configure({
        keyboardCondition: null
      });
  }
  clearInput() {
    (<any>this.inputElt).value = '';
    this.inputElt.bindValue = '';
    this.clearResults();
  }
  clearResults() {
    if (this.occurencesListe)
      this.splice('occurencesListe', 0, this.occurencesListe.length);
    this._displayResults = false;
  }

  tadiavo() {
    this.clearResults();
    this._waiting = true;
    getFihirana().toPromise()
      .then((hiraJson: Fihirana.FihiranaJson) => {
        const occurenceList: Occurence[] = [];
        for (let idHira of Object.keys(hiraJson)) {
          const hira = new Hira(hiraJson[idHira]);
          const hita = hira.searchInLyrics(this.pattern);
          if (hita) {
            occurenceList.push(new Occurence(idHira, hita.match));
          }
        }
        this._displayResults = occurenceList.length > 0;
        this._noResults = occurenceList.length === 0;
        return occurenceList;
      })
      .then((occurenceList) => {
        this._waiting = false;
        // changes to occurencesListe must happen after dom-if has rendered
        // because they don't reflect to dom-repeat as long as dom-if is false
        // (otherwise call `render()` to force synchronous rendering)
        // https://github.com/Polymer/polymer/issues/4818
        if (occurenceList.length > 0) {
          this.push('occurencesListe', ...occurenceList)
        }
        return fromEventOnce('dom-change', this.shadowRoot.querySelector('.dom-if-results'));
      });
  }

  goToHira(e: MouseEvent & {model: TemplateInstanceBase & {item: Occurence}}) {
    let hira = e.model.item.hira;
    if (hira.search(/ffpm/i) !== -1)
      hira = hira.replace(/ffpm_/, '');
    else
      hira = hira.replace(/_/, '');
    const rechercheHira = document.querySelector('recherche-hira') as RechercheHira;
    if (rechercheHira) {
      rechercheHira.hiraValue = hira;
      rechercheHira.insertNewHira = false;
      rechercheHira.open();
      fromEventOnce('closed', rechercheHira)
        .then(() => {
          debugger;
          rechercheHira.insertNewHira = true
          rechercheHira.hiraValue = '';
        });
    }
  }
}

// associate hira number to the string matching the pattern
// for use in occurencesListe
class Occurence {
  str: string;
  constructor(public hira: string, ligneString: string) {
    this.str = ligneString;
  }
}

export class Hira {
  id: string = '';
  num: Number = -1;
  title: string = '';
  version: Number = -1;
  lyrics: Lyrics[] = [];
  constructor(raw: Fihirana.HiraJson) {
    for (let k in this as Hira) {
      if (raw[k] != undefined) {
        this[k] = raw[k];
      }
    }
  }
  getLyrics() {
    return this.lyrics;
  }
  searchInLyrics(pattern: RegExp): {idHira: string,verseNum: Number,match: string}|null  {
    // const sentencePattern = new RegExp(`(^|\n).*${pattern.source}.*(\n|$)`, pattern.flags);
    for (let verseMeta of this.lyrics) {
      const found = pattern.exec(removeAccents(verseMeta.lyrics));
      if (found?.length > 0) {
        const lyrics = verseMeta.lyrics.slice();
        let specificMatchLength: number;
        let indexOfSpecificMatch: number;
        if (found.length === 2) {
          indexOfSpecificMatch = found.index + found[0].indexOf(found[1]);
          specificMatchLength = found[1].length;
        } else {
          indexOfSpecificMatch = found.index;
          specificMatchLength = found[0].length;
        }
        const match =
          lyrics.slice(0, indexOfSpecificMatch)
          + lyrics.substr(indexOfSpecificMatch, specificMatchLength).bold()
          + lyrics.substr(indexOfSpecificMatch + specificMatchLength);
        return {
          idHira: this.id,
          verseNum: verseMeta.num,
          match
        };
      }
    }
    return null;
  }
}

type HiraType = 'verse' | 'refrain' | 'other';

export interface Lyrics {
  num: Number;
  lyrics: string;
  type: HiraType;
}