import { css, customElement, html, LitElement, property, PropertyValues } from 'lit-element';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';
import '@material/mwc-ripple';

export interface FPMAOHiraKey {
   label: string;
   hint: string;
   type: 'prefix' | 'number' | 'edition';
   value: string;
}

export interface FPMAOKeyPressEventDetail {
   value: string;
   type: FPMAOHiraKey['type'];
}

@customElement('fpmao-hira-key')
export class HiraKeyElement extends LitElement {
   static get styles() {
      return css`
         :host {
            display: flex;
            justify-content: center;
            align-items: center;
            width: var(--fpmao-keyboard-key-size);
            height: var(--fpmao-keyboard-key-size);
            font-size: calc(var(--fpmao-keyboard-key-size) * 0.5);
            color: var(--fpmao-keyboard-key-color);
            fill: currentColor;
            border-radius: 5px;
            background: var(--fpmao-keyboard-key-bg);
            box-shadow: var(--fpmao-keyboard-key-shadow);
            padding: calc(var(--fpmao-keyboard-key-size) * 0.25);
            cursor: pointer;
         }
      `
   }

   render() {
      return html`
      <mwc-ripple></mwc-ripple>
      <slot></slot>
      `;
   }

   @property({ type: Object })
   key: FPMAOHiraKey;

   firstUpdated(changes: PropertyValues) {
      super.firstUpdated(changes);
      fromEvent(this, 'click').pipe(
         map(() => new CustomEvent<FPMAOKeyPressEventDetail>('hira-key-pressed', {
            detail: { value: this.key.value, type: this.key.type },
            bubbles: true,
            composed: true,
         })),
      ).subscribe(event => {
         this.dispatchEvent(event);
      })
   }
}