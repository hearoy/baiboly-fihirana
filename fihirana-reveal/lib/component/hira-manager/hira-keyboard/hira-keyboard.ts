import '@material/mwc-icon';
import { css, customElement, html, LitElement, property, PropertyValues } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';
import { unsafeHTML } from 'lit-html/directives/unsafe-html';
import { fromEvent } from 'rxjs';
import "./hira-key";
import { FPMAOHiraKey, FPMAOKeyPressEventDetail } from './hira-key';

export interface FPMOKeyboardPressedEventDetail {
   value: string;
}
const KEYS: FPMAOHiraKey[] = populateKeys();
function populateKeys() {
   const keys: FPMAOHiraKey[] = [];
   // prefixes
   keys.push(
      {
         label: 'FF',
         hint: 'Fihirana Fanampiny',
         type: 'prefix',
         value: 'ff'
      },
      {
         label: 'An',
         hint: 'Antema',
         type: 'prefix',
         value: 'an'
      },
      {
         label: 'FP',
         hint: 'Fanekem-pinoana',
         type: 'prefix',
         value: 'fp'
      },
   );
   // 1 to 9
   new Array(9).fill(0).forEach((_, i) => {
      keys.push({
         label: (++i).toString(),
         hint: '',
         type: 'number',
         value: i.toString(),
      })
   })
   // 0
   keys.push({
      label: '0',
      hint: '',
      type: 'number',
      value: '0',
   })
   return keys;
}

const KEYS_EDITION: FPMAOHiraKey[] = [
   {
      label: '<mwc-icon><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M22 3H7c-.69 0-1.23.35-1.59.88L0 12l5.41 8.11c.36.53.9.89 1.59.89h15c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-3 12.59L17.59 17 14 13.41 10.41 17 9 15.59 12.59 12 9 8.41 10.41 7 14 10.59 17.59 7 19 8.41 15.41 12 19 15.59z"/></svg></mwc-icon>',
      hint: 'Fafàna ny farany',
      type: 'edition',
      value: '{backspace}'
   },
   {
      label: '<mwc-icon><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M14.59 8L12 10.59 9.41 8 8 9.41 10.59 12 8 14.59 9.41 16 12 13.41 14.59 16 16 14.59 13.41 12 16 9.41 14.59 8zM12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/></svg></mwc-icon>',
      hint: 'Fafàna daholo',
      type: 'edition',
      value: '{clear}'
   },
   {
      label: '<mwc-icon><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M14 10H2v2h12v-2zm0-4H2v2h12V6zm4 8v-4h-2v4h-4v2h4v4h2v-4h4v-2h-4zM2 16h8v-2H2v2z"/></svg></mwc-icon>',
      hint: 'Manaraka',
      type: 'edition',
      value: ', '
   },
   {
      label: '<mwc-icon><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="18px" height="18px"><path d="M0 0h24v24H0z" fill="none"/><path d="M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z"/></svg></mwc-icon>',
      hint: 'Ampiana andininy',
      type: 'edition',
      value: ':'
   },
];


@customElement('fpmao-hira-keyboard')
export class HiraKeyboardElement extends LitElement {
   keys = KEYS;
   keysEdition = KEYS_EDITION;

   @property({ type: String, attribute: 'input-value' })
   inputValue: string;

   static get styles() {
      return css`
         :host {
            display: grid;
            grid-template-columns: 3fr 1fr;
            grid-gap: 0.5rem;
            margin: 1rem 0;
         }
         .hira-key-number:nth-last-of-type(1) {
            grid-column: 2 / 3;
         }
         .hira-key-numbers-wrapper,
         .hira-key-edition-wrapper {
            grid-gap: 0.5rem;
         }
         .hira-key-numbers-wrapper {
            display: grid;
            grid-template-columns: repeat(3, 1fr);
         }
         .hira-key-edition-wrapper {
            display: grid;
            grid-template-rows: repeat(5, 1fr);
         }
         .hira-key-edition-next {
            grid-row: 4;
         }
         .hira-key-edition-andininy {
            grid-row: 5;
         }
      `;
   }

   render() {
      return html`
      <div class="hira-key-numbers-wrapper">
         ${this.keys.map(key => html`
            <fpmao-hira-key .key=${key}
                            title=${key.hint}
                            class=${classMap({
                              'hira-key': true,
                              'hira-key-prefix': key.type === 'prefix',
                              'hira-key-number': key.type === 'number',
                           })}>${key.label}</fpmao-hira-key>
         `)}
      </div>
      <div class="hira-key-edition-wrapper">
         ${this.keysEdition.map(key => html`
            <fpmao-hira-key .key=${key}
                            title=${key.hint}
                            class=${classMap({
                              'hira-key': true,
                              'hira-key-edition': key.type === 'edition',
                              'hira-key-edition-next': key.value === ', ',
                              'hira-key-edition-andininy': key.value === ':',
                            })}>${unsafeHTML(key.label)}</fpmao-hira-key>
         `)}
      </div>
      `;
   }

   private readonly patterns = new class HiraPatternMatching {
      lastMatch: { matches: boolean, found: RegExpExecArray, formatted: string };
      any(text: string): boolean {
         return this.ff(text).matches
            || this.ffpm(text).matches
            || this.an(text).matches
            || this.fp(text).matches;
      }
      anyPrefixed(text: string): boolean {
         return this.ff(text).matches
            || this.an(text).matches
            || this.fp(text).matches;
      }
      ff(text: string) {
         return this.match(/ff *\d{1,2}(?!\d)/i, text);
      }
      ffpm(text: string) {
         return this.match(/\b\d{1,3}(?!\d)/i, text);
      }
      an(text: string) {
         return this.match(/an *\d{1,2}(?!\d)/i, text);
      }
      fp(text: string) {
         return this.match(/fp *[1-5](?!\d)/i, text);
      }
      private match(pattern: RegExp, text: string) {
         const withoutAndininy = text.replace(/:.*/, '');
         const matches = pattern.exec(withoutAndininy);
         return this.lastMatch = {
            matches: !!matches?.length,
            found: matches,
            formatted: matches?.[0].replace(/ /g, ''),
         };
      }
   };

   firstUpdated(changes: PropertyValues) {
      super.firstUpdated(changes);
      fromEvent<CustomEvent<FPMAOKeyPressEventDetail>>(this, 'hira-key-pressed')
         .subscribe(event => {
            const { detail: { value: keyValue, type } } = event;
            const parts = this.inputValue?.split(/ *, */);
            const lastPart = parts?.slice(-1)[0];
            if (!this.inputValue || this.inputValue.trim().length === 0) {
               return this.dispatchKeyboardPressedEvent(keyValue);
            }
            else if (type === 'prefix') {
               if (lastPart.length === 0) { // ends with ", "
                  return this.dispatchKeyboardPressedEvent(keyValue);
               } else if (this.patterns.any(lastPart)) { // previous value matches a known pattern
                  return this.dispatchKeyboardPressedEvent(`, ${keyValue}`); // start a new hira with the pressed prefix
               }
            }
            else if (type === 'number') {
               const lastPart = parts.filter(p => p !== '').slice(-1)[0];
               if (this.patterns.anyPrefixed(lastPart + keyValue)) {
                  return this.dispatchKeyboardPressedEvent(keyValue);
               } else if (this.patterns.anyPrefixed(lastPart)) {
                  return this.dispatchKeyboardPressedEvent(`, ${keyValue}`);
               } else if (this.patterns.ffpm(lastPart).matches) {
                  if (this.patterns.ffpm(lastPart + keyValue).matches) { // adding the pressed key still matches a ffpm hira
                     return this.dispatchKeyboardPressedEvent(keyValue); // allow the key as is
                  } else { // start a new ffpm hira
                     return this.dispatchKeyboardPressedEvent(`, ${keyValue}`);
                  }
               }
            }
            else if (type === 'edition') {
               const lastPart = parts.filter(p => p !== '').slice(-1)[0];
               if (keyValue === ':') {
                  if (lastPart.match(/:.*\d$/)) { // has a ":" and ends with a number
                     return this.dispatchKeyboardPressedEvent('-');
                  } else if (!lastPart.match(':') && !this.patterns.fp(lastPart)) { // has a ":"
                     return this.dispatchKeyboardPressedEvent(':');
                  }
               }
               return this.dispatchKeyboardPressedEvent(keyValue);
            }
         })
   }

   private dispatchKeyboardPressedEvent(keyValue: string): void {
      this.dispatchEvent(new CustomEvent<FPMOKeyboardPressedEventDetail>('hira-keyboard-pressed', {
         detail: { value: keyValue },
         bubbles: true,
         composed: true,
      }));
   }

}