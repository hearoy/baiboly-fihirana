import { customElement, property } from "@polymer/decorators";
import "@polymer/iron-icon/iron-icon";
import "@polymer/paper-button/paper-button";
import "@polymer/paper-input/paper-input";
import "@polymer/paper-spinner/paper-spinner";
import "@polymer/paper-styles/shadow";
import { html, PolymerElement } from "@polymer/polymer";
import "@webcomponents/shadycss/entrypoints/apply-shim.js";
import "res/icons/myicons";
import { of } from 'rxjs';
import { exhaustMap, filter } from 'rxjs/operators';
import { BaibolyDispatcher } from "src/baiboly-dispatcher/baiboly-dispatcher";
import { getFihirana } from 'src/fihirana/fihirana-puller/fihirana-puller';
import { acknowledgeMessage, answerWithCorrection, getChannelHira } from 'src/inter-communication/intercom';
import { ClearSlideEvent, CLEAR_SLIDE_EVENT } from 'src/model/slide';
import { addEventListenerTool, checkForReveal, fromEventOnce, setTimeoutPromise, untilDisconnected } from 'src/tools';
import { MetricsSvc } from 'src/tools/metrics';
import Typed from "typed.js/lib/typed.min.js";
import '../hr-hira/hr-hira';
import type { Fihirana, HrHira } from '../hr-hira/hr-hira';
import { FPMAOKeyPressEventDetail } from './hira-keyboard/hira-key';
import './hira-keyboard/hira-keyboard';

declare var window: MyWindow;

type HiraWithHighlights = { hira: Fihirana.HiraJson, andininy: number[] }
export type HiraWithHighlightsAndIndex = { hira: Fihirana.HiraJson, andininy: number[], index: number }
type HiraChangeActions = "clearAll" | "clear" | "insert" | "append";

export type HiraListChangeEventDetail<Action extends HiraChangeActions = HiraChangeActions> = 
    {
      hiraList: HiraWithHighlightsAndIndex[];
      index: Action extends "clearAll" ? never : number;
      action: Action;
   };

   @customElement('hira-manager')
   /**
    * `hira-manager`
    * retrieves 1 hira
    * 
    * @fires HiraManager#sectionsAdded
    * @customElement
    * @polymer
    * @demo demo/index.html
    */
   export class HiraManager extends PolymerElement {
      
      static get template() {
         return html`
            <style>
       :host {
        display: flex;
        flex-flow: row wrap;
        max-height: 100%;
        justify-content: center;
        align-items: center;
        align-content: start;
        font-size: 1rem;
        flex: 1;

        --paper-button: {
          margin: 1ex;
          text-transform: none;
        }
        ;
        
        --paper-button-disabled: {
          background: #eaeaea;
          color: #a8a8a8;
        }
      }
      
      :host .paper[elevation="1"] {
        @apply --shadow-elevation-2dp;
      }
      
      .overlay_spinner {
        position: absolute;
        top:0;
        left:0;
        right:0;
        bottom:0;
        background-image: radial-gradient(farthest-corner at center, rgba(0,0,0,0.3) 0, rgba(200,200,200,0.1));
        visibility: hidden;
        opacity: 0;
        transition-delay: 0s;
        transition-duration: 250ms;
        transition-property: visibility, opacity;
        transition-timing-function: ease-in-out;
        z-index: 1;
        will-change: visiility, opacity;
      }
      .overlay_spinner--dark {
        opacity: 1;
        visibility: visible;
      }

      paper-spinner {
        position: absolute;
        --paper-spinner-stroke-width: 10px;
        top: calc( 50% - 20vh / 2 );
        left: calc( 50% - 20vh / 2 );
        height: 20vh;
        width: 20vh;
        filter: drop-shadow(10px 10px 7px rgba(200,100,0,0.4));
      }

      .main-container {
        padding: 1ex;
        display: flex;
        flex-flow: row wrap;
        justify-content: center;
        align-items: center;
        align-content: center;
        height: 50%;
        flex: 1 0 100%;
        max-width: 600px;
        margin: 0 1px;
        transition-delay: 0s;
        transition-duration: 250ms;
        transition-property: filter;
        transition-timing-function: ease-in-out;
        filter: blur(0);
        will-change: filter;
      }
      .overlay_spinner--dark ~ .main-container {
        filter: blur(3px);
      }
      @media (min-width: 410px) {
         .main-container-wrapper {
            height: 50%;
         }
      }
      .main-container-wrapper {
         height: 40%;
      }
      .main-container-wrapper,
      keyboard-wrapper {
         display: flex;
         justify-content: center;
         align-items: center;
         flex: 1 0 100%;
      }

      .enRelief {
        --paper-button: {
          color: #fff;
          background-color: rgba(38, 73, 147, .8);
          text-transform: none;
        }
        ;
      }

       :host paper-button.enRelief:hover {
        background-color: #264993;
      }

       :host paper-button {
        transition: background-color 0.3s ease;
      }

       :host paper-button:hover {
        background-color: rgba(38, 73, 147, .2);
      }

      .center-container {
        flex: 1 0 100%;
        display: flex;
        flex-flow: row wrap;
        justify-content: center;
      }

      #input {
        text-align: left;
        max-width: 15em;
        flex: 1;
      }

    </style>
    <div class$="[[_spinnerStyler(_waiting)]]">
      <paper-spinner active="[[_waiting]]" style$="[[_spinnerStyle]]"></paper-spinner>
    </div>
    <div class="main-container-wrapper">
       <div class="paper main-container" elevation$="[[_elevation(inDialog)]]">
         <!-- <h3 class="title">Lisitra-na hira</h3> -->
         <div class="center-container">
         <paper-input id="input" value="{{inputHira}}"  error-message="XXX:Y-Y-Y, anXX, na ffXX" allowed-pattern="[anfp0-9, :\-]"
            pattern="[[_pattern]]" auto-validate stop-keyboard-event-propagation="true" on-blur="_onBlur" on-keydown="_onKeydown" on-value-changed="_onValueChanged"></paper-input>
         </div>
         <paper-button hidden$="[[! inDialog]]" on-click="leaveDialog">Miala</paper-button>
         <paper-button on-tap="clearHiras" hidden$="[[ inDialog]]">Fafàna</paper-button>
         <paper-button class="enRelief" disabled="[[_disabled]]" id="processButton" on-tap="processButton">
         <iron-icon icon="myicons:find-in-page"></iron-icon>
         [[__labelProcessButton(insertNewHira)]]</paper-button>
      </div>
    </div>
    <div class="keyboard-wrapper">
      <fpmao-hira-keyboard on-hira-keyboard-pressed="_onKeyboardPressed"
                           input-value="[[inputHira]]"></fpmao-hira-keyboard>
   </div>
         `;
      }


      _isBig: boolean;
      label: any;
      invalid: boolean;
      baibolyDispatcher: BaibolyDispatcher;
      _waiting: boolean;
      __request: Request;
      _disabled: boolean;
      @property({type: Boolean})
      insertNewHira = false;
      @property({type: Array, notify: true})
      hiraList: HiraWithHighlightsAndIndex[];
      inputHira: string;
      isCurrent: boolean;
      @property({type: Boolean, reflectToAttribute: true})
      inDialog = false;
      /**
       * type {Hira} 
       hiraObj: {
         type: Object,
         computed: 'getHira(hira)'
        },
        */
      /**
       * @type {String} 
       */
      hira: string;
      jsonURL = '/fihirana.json';
      private metricsSvc: MetricsSvc;
      private setByMessage: boolean;
      private hasRenderedHiraOnce: boolean;

      
      static get is() {
         return 'hira-manager';
      }
      static get properties() {
         return {
            _disabled: {
               type: Boolean,
               value: ''
               //computed: '_disableButton(inputHira)' // if computed, it isn't sync'd to a dynamic assignement of inputHira
               // use an observer on inputHira instead
            },
            inputHira: {
               type: String,
               value: '',
               observer: '_inputHiraChanged'
            },
            isCurrent: {
               type: Boolean,
               value: false
            },
            /**
             * @type {String} 
             */
            hira: {
               type: String,
               value: '1'
            },
         };
      }
      //*** PROPERTIES
      get _pattern() {
         const regExp = '( *(an|ff|fp)?\\d+( ?: ?\\d(-\\d)*)? *,?)+';
         return regExp;
      }

      static get CLASS_AUTO_ADD() {
         return 'auto-added-hira';
      }
      get inputElement() {
         return (<any>this.$.input).inputElement.inputElement;
      }

      //*** METHODS

      
      disconnectedCallback() {
         super.disconnectedCallback();
         
      }

      _disableButton(inputHira) {
         // this.$.input.invalid doesn't validate ffX
         return inputHira == '' || !new RegExp(this._pattern).test((<HTMLInputElement>this.$.input).value);
      }

      private _elevation(inDialog: boolean) {
         return inDialog ? 0 : 1;
      }

      _inputHiraChanged(newV, oldV) {
         this.set('_disabled', this._disableButton(newV));
      }

      __labelProcessButton(insertNewHira: boolean) {
         return insertNewHira ?  'Atao manaraka' : `Atao amin'ny farany`;
      }

      _onBlur() {
         const reveal = window.Reveal;
         if(reveal)
            reveal.configure({
               keyboardCondition: null
            });
      }

      _onFocus(e) {
         const reveal = window.Reveal || '';
         if(reveal) // keep Reveal.js from acting on keypresses
            reveal.configure({
               keyboardCondition() { return false },
            });
      }

      _onKeydown(e) {
         //console.log(e.keyCode);
         if(e.keyCode === 13 && !this.invalid)
            (<HTMLButtonElement>this.$.processButton).click();
      }

      _onKeyboardPressed(e: CustomEvent<FPMAOKeyPressEventDetail>) {
         const key = e.detail.value;
         
         if (key.startsWith('{')) {
            const action = key.match(/\{(.*)\}/)[1];
            switch (action) {
               case 'backspace':
                  let oldLength = this.inputHira.length;
                  this.inputHira = this.inputHira.replace(/[, ]*$/, '');
                  if (oldLength === this.inputHira.length) {
                     this.inputHira = this.inputHira.slice(0, -1);
                  }
                  break;
               case 'clear':
                  this.inputHira = '';
                  break;
               default:
                  break;
            }
         } else {
            this.inputHira += key;
         }
      }

      _onValueChanged(e: Event) {
         const element = <HTMLInputElement>e.currentTarget;
         const length = element.value.length;
         if(length > 25) {
            if(this._isBig)
               return;
            else {
               this._isBig = true;
               element.style.maxWidth = 'none';
            }
         } else {
            if(this._isBig) {
               this._isBig = false;
               return element.style.maxWidth = '';
            } else {
               return;
            }
         }
      }

      _spinnerStyler(waiting) {
         if(waiting) {
            return 'overlay_spinner overlay_spinner--dark';
         } else
            return 'overlay_spinner';
      }

      constructor() {
         super();
         this.metricsSvc = new MetricsSvc();
      }

      ready() {
         super.ready();
         if(!this.insertNewHira) {
            checkForReveal().then(_reveal => {
               this.addListeners();
               this.processQueryString();
            });
         }
         /* initialize spinner to off */
         this._waiting = false;
         const typedOptions: any = {
            strings: ['ohatra : 530:1-3-4,an5', 'ohatra : ff26:1-2, ^100 351:2-3, an1', 'ohatra : 701:1-2-3, fp1, ff1:2', 'ohatra : 254:1, 444:2, 303:2-3'],
            typeSpeed: 50,
            loop: true,
            contentType: null,
            attr: 'label',
            backDelay: 1500,
            backSpeed: 30,
            showCursor: false,
            autoInsertCss: false
         };
         this.label = new Typed(this.$.input, typedOptions);

         this.baibolyDispatcher = document.querySelector('baiboly-dispatcher');

         fromEventOnce('hira-list-change', this, { untilDisconnected: this }).then(() => {
            this.hasRenderedHiraOnce = true;
         })
      }

      private addListeners() {
         addEventListenerTool(this.shadowRoot.querySelector('.main-container'), 'click', this.focusOnClick.bind(this), { passive: true })
            .untilDisconnected(this);
         addEventListenerTool(window, 'hashchange', this.updateQueryString.bind(this, 0), { passive: true })
            .untilDisconnected(this);
         addEventListenerTool(window, CLEAR_SLIDE_EVENT, this.clearSlide.bind(this), { passive: true }).untilDisconnected(this);
         if (this.inDialog === false) {
            this.subscribeToHiraTopic();
         }
      }

      clearHiras(e) {
         this.metricsSvc.sendEvent({
            action: 'click',
            category: 'fihirana',
            label: 'clear hira sequence',
         });
         // this.splice('hiraList', 0, this.hiraList.length);
         this.hiraList.splice(0, this.hiraList.length);
         this.fireHiraListChangeEvent({ action: 'clearAll' });
         setTimeoutPromise(() => {
            Reveal.sync();
            this.updateQueryString(0);
         }, 0);
      }

      clearSlide(event: ClearSlideEvent): Promise<void> {
         const { coords, type } = event.detail;
         if (type === 'hira') {
            const currentSlide = window.Reveal.getCurrentSlide();
            const slidesWrapper = currentSlide.closest('.slides');
            const totalHorizontal = slidesWrapper.querySelectorAll(':scope > section').length;
            const indexOfSlideToDelete = this.hiraList.length - totalHorizontal + coords.h;
            // this.splice('hiraList', indexOfSlideToDelete, 1);
            this.hiraList.splice(indexOfSlideToDelete, 1);
            this.fireHiraListChangeEvent({ action: 'clear', index: indexOfSlideToDelete });
         } else {
            return Promise.resolve();
         }
      }

      /**
       * Create hira objects from a sequence of hira given as a string, comma separated.
       * Insensitive to spaces between hira's.  
       * e.g. '512, an12,ff26 , 31' => chain of Promise<Hira> 
       * 
       * @param {string} hirasString formatted as id:andininy-andininy, id:andininy 
       * @returns {Promise<any[]>} the chain of promises fulfilled with
       *  object versions of the hira's and the andininy to highlight
       * @memberof HiraManager
       */
      createHiraObjs(hirasString: string): Promise<HiraWithHighlights[]> {

         /* set off the spinner */
         this.spinnerOn();
         const hirasArr = hirasString.replace(' ', '').split(',');
         // hirasArr.map(val => this.getHira(val));

         /**
          * @type {Promise<any[]>}
          */
         const hirasAP = new Promise<HiraWithHighlights[]>((resolve, reject) => {
            const acc = [];
            const chainOfPromises = hirasArr.reduce((prev, curr) => {
               const andininyRegxp = curr.match(/:(\d)(-\d)*/);
               /** the andininy {number} to highlight */
               const andininy: number[] = this.extractAndininys(curr);
               return prev.then(_ => this.getHira(curr))
                  .then(o => {
                     for(let item of o) {
                        acc.push({ hira: item, andininy })
                     }
                  });
            }
               , Promise.resolve());
            chainOfPromises.then(_ => {
               if(acc.length > 0) {
                  resolve(acc)
               } else {
                  reject('empty')
               }
            });
         })
         // return Promise.all(hirasArr.map(this.getHira.bind(this)));
         // let hirasP = hirasArr.reduce((prev, curr) => {
         //   return prev.then(_ => this.getHira(curr));
         // }, Promise.resolve());

         return hirasAP;
      }

      extractAndininys(hiraStr: string): number[] {
         const andininyRegxp = hiraStr.match(/:(\d)(-\d)*/);
         /** the andininy {number} to highlight */
         if (andininyRegxp) {
            return andininyRegxp[0].replace(/ *: */, '').split('-').map(val => Number(val)) || [];
         } else
            return [];
      }

      private focusOnClick(e) {
         const path = e.composedPath();
         if(path.some(elt => elt.tagName && (elt.matches('paper-button') || elt.matches('paper-input'))))
            return;
         else if((<any>this.$.input).focused)
            (<HTMLInputElement>this.$.input).blur();
         else(<HTMLInputElement>this.$.input).focus()
      }

      /**
       * Format a hira given as user-readable into the id of the hira in the (json) database
       * @param h hira as input by the user; format (ff|ffpm|an|fp)?\d+
       */
      formatHira(h: string): string {
         h = h.trim();
         const ff = h.match(/ff(?!pm)/i);
         const antema = h.match(/an/i);
         const fanekempinoana = h.match(/^fp(?!m)/i);
         const numHiraRegx = h.match(/\d+/);
         if (numHiraRegx === null) {
            throw new Error('Tsy vaovaky izany hira "' + h + '" izany');
         }
         let numHira = numHiraRegx[0].replace(/^0+/, '');

         if(ff) { // fihirana fanampiny
            numHira = 'ff_' + numHira;
         } else {
            if(antema) {
               numHira = 'antema_' + numHira;
            } else if(fanekempinoana) {
               numHira = 'fp_' + numHira;
            } else {
               numHira = 'ffpm_' + numHira;
            }
         }
         return numHira;
      }

      /**
       * 
       * @param {Fihirana.FihiranaJson} obj contains all the hiras
       * @param {string} hiraString the id of the hira to retrieve
       * @returns {Fihirana.HiraJson[]} array of pojo's containing every version of the hira whose id is given in paramaeter
       * @memberof HiraManager
       */
      getFromObj(obj: Fihirana.FihiranaJson, hiraString: string): Fihirana.HiraJson[] {
         try {
            const numHira = this.formatHira(hiraString);
            let hira: Fihirana.HiraJson[] = [obj[numHira]];
            if (hira[0] === undefined) {
               console.error('Hira %s not found', numHira);
               alert(`Hira ${hiraString} not found`)
               return [{id: hiraString, lyrics: [{lyrics: '!!! Tsy hita !!!', type: 'refrain', num: '-1'}], num: numHira, title: 'Tsy hita !', type: 'antema', version: []}]
            }
            const version = hira[0].version;
            if (version.length > 0) {
               hira = version.map(key => obj[key]);
            }
            for (let h of hira) {
               h.lyrics = h.lyrics.map(val => {
                  val.lyrics = val.lyrics.trim();
                  return val;
               });
            }
            return hira;
         } catch (e) {
            throw e;
         }
      }

      getHira(which: string): Promise<Fihirana.HiraJson[]> {
         return getFihirana().toPromise()
            .then(hs => this.getFromObj(hs, which)).catch(e => {
            console.error(e);
            return Promise.reject([]);
         });
      }

      leaveDialog() {
         this.dispatchEvent(new CustomEvent('closed', { bubbles: true }));
      }

      processButton(e: MouseEvent) {
         this.metricsSvc.sendEvent({
            action: 'click',
            category: 'fihirana',
            label: 'select hira sequence',
         });
         

         /** prevent from re-focusing the input because of the listener **/
         e.stopPropagation();
         this.processInput({ input: this.inputHira, insert: this.insertNewHira })
            .then(({ insertPointSlideIndex, lengthAdded }) => {
               (<HTMLInputElement>this.$.input).blur();
               if (window.Reveal) {
                  /* Sync window.Reveal to update navigation arrows etc. */
                  window.Reveal.sync();
                  this.spinnerOff(0);
                  /** go to the first hira of the newly added content **/
                  window.Reveal.slide(insertPointSlideIndex + 1);
               }
               /**
                * Fired when sections containing hiras are added to the document.
                * 
                * @event HiraManager#sectionsAdded
                * @type {object}
                */
               this.dispatchEvent(new CustomEvent('sections-added', { composed: true, bubbles: true }));
            })
            .catch(reason => {
               console.error(reason);
               this.spinnerOff(50);
            });
      }

      private processInput({ input, insert }: { input: string; insert: boolean; }) {
         return this.createHiraObjs(input)
            .then((bundles: HiraWithHighlights[]) => {
               if (window.Reveal) {
                  /* DOM dependent */
                  const currentSlide = window.Reveal.getCurrentSlide();
                  const slidesWrapper = currentSlide.closest('.slides');
                  const totalHorizontal = slidesWrapper.querySelectorAll(':scope > section').length;
                  const indexOfFirstHira = totalHorizontal - this.hiraList.length;
                  // at least, consider the first hira as the current slide
                  const indexOfCurrentSlide = Math.max(window.Reveal.getIndices().h, indexOfFirstHira);
                  /* /DOM dependent */
                  let indexOfInsertPoint: number;
                  if (insert) {
                     indexOfInsertPoint = this.hiraList.length - totalHorizontal + indexOfCurrentSlide + 1;
                     indexOfInsertPoint = Math.max(0, indexOfInsertPoint);
                  }
                  else { // append
                     indexOfInsertPoint = this.hiraList.length;
                  }
                  // this.splice('hiraList', indexOfInsertPoint, 0, ...bundles.map((v, index) => ({ ...v, index: index + indexOfInsertPoint })));
                  this.hiraList.splice(indexOfInsertPoint, 0, ...bundles.map((v, index) => ({ ...v, index: index + indexOfInsertPoint })));
                  this.fireHiraListChangeEvent({ index: indexOfInsertPoint, action: insert ? 'insert' : 'append' });
                  /* wait for dom-if and dom-repeat to render asynchronously*/
                  return setTimeoutPromise(() => ({ insertPointSlideIndex: indexOfFirstHira + indexOfInsertPoint - 1, lengthAdded: bundles.length }), 0);
               }
            });
      }

      private fireHiraListChangeEvent({ action }: { action: 'clearAll'; }): void;
      private fireHiraListChangeEvent({ index, action }: { index: number; action: 'insert' | 'append' | 'clear' }): void;
      private fireHiraListChangeEvent<A extends 'insert' | 'append' | 'clear' | 'clearAll'>({ index, action }: { index?: number; action: A; }): void {
         const detail: HiraListChangeEventDetail<A> = {
            hiraList: this.hiraList,
            action,
            // @ts-expect-error
            index: action === 'clearAll' ? null : index
         };
         const event = new CustomEvent<HiraListChangeEventDetail<A>>('hira-list-change',
            {
               bubbles: true, composed: true, detail
            });
         this.dispatchEvent(event)
      }

      /**
      * 
      * add hiras from the queryString
      * @returns void
      * @memberof HiraManager
      */
      processQueryString() {
         const searchParams = (new URL(location.href)).searchParams;
         const listOfHira = searchParams.get('hiras');
         const position = searchParams.get('pos');
         let title = searchParams.get('title');
         if(title != null && title != 'null')
            title.replace('+', ' ');
         else
            title = document.title;
         if(!listOfHira) {
            this.spinnerOn();
            Promise.resolve().then(_ => {
               if(this.baibolyDispatcher)
                  return this.baibolyDispatcher.processQueryString();
               else {
                  this.baibolyDispatcher = document.querySelector('baiboly-dispatcher');
                  if(this.baibolyDispatcher)
                     return this.baibolyDispatcher.processQueryString();
               }
            })
               .then(_ => {
                  window.Reveal?.sync();
               })
               .then(_ => {
                  /* update document's title */
                  document.title = title;
                  this.spinnerOff(100);
                  if (!this.setByMessage) {
                     window.Reveal?.slide(Number(position));
                  }
               })
               
         }
         else {


            this.createHiraObjs(listOfHira)
               .then((bundles: HiraWithHighlights[]) => {
                  console.assert(this.hiraList?.length === 0, 'Tokony tsy nisy ninoninona tao anatiny ny "hiraList"')
                  // this.push('hiraList', ...bundles.slice().map((v, index) => ({ ...v, index })));
                  this.hiraList.push(...bundles.slice().map((v, index) => ({ ...v, index })));
                  this.fireHiraListChangeEvent({ action: 'append', index: this.hiraList.length - bundles.length });
                  /* wait for dom-if and dom-repeat to render asynchronously*/
                  return setTimeoutPromise(() => {}, 0);
               })
               .then(() => {
                  if(this.baibolyDispatcher)
                     return this.baibolyDispatcher.processQueryString();
                  else {
                     this.baibolyDispatcher = document.querySelector('baiboly-dispatcher');
                     if(this.baibolyDispatcher)
                        return this.baibolyDispatcher.processQueryString();
                  }
               })
               .then(() => {
                  if(window.Reveal) {
                     /* Sync window. to update navigation arrows etc. */
                     window.Reveal.sync();
                  }
               })
               .then(() => {
                  /* update document's title */
                  document.title = title;
                  this.spinnerOff(100);
                  if (!this.setByMessage) {
                     window.Reveal?.slide(Number(position));
                  }

                  /**
                      * Fired when sections containing hiras are added to the document.
                      * 
                      * @event HiraManager#sectionsAdded
                      * @type {object}
                      */
                  this.dispatchEvent(new CustomEvent('sections-added', { composed: true, bubbles: true }));
               })
               .catch(e => {
                  console.error(e);
                  this.spinnerOff(50);
               });
         }
      }

      spinnerOff(delay: number = 0) {
         window.setTimeout(() => { this._waiting = false; }, delay);
      }
      spinnerOn() {
         window.setTimeout(() => { this._waiting = true; }, 0);
      }

      private subscribeToHiraTopic() {
         getChannelHira().pipe(
            filter(({ action }) => action === 'go'),
            filter(({ payload }) => payload?.match(/(ff(pm)?|fp|antema)_\d+(:\d+(-\d+)?)?/)?.length > 0),
            exhaustMap((event, idx) => {
               if (idx === 0 && !this.hasRenderedHiraOnce) {
                  return fromEventOnce('hira-list-change', this).then(() => event);
               } else {
                  return of(event);
               }
            }),
            untilDisconnected(this),
         ).subscribe((message) => {
            const { payload: target } = message;
            const mainDispatcherIndex = Reveal.getIndices(this.closest('section')).h;
            const indexFound = this.hiraList.findIndex(hira => {
               const searchToken = hira.hira.id + (hira.andininy.length ? ':' : '') + hira.andininy.join('-');
               return searchToken === target;
            });
            if (indexFound > -1) {
               Reveal.slide(mainDispatcherIndex + indexFound + 1);
               acknowledgeMessage(message);
            } else {
               this.processInput({ input: target, insert: false })
                  .then(({insertPointSlideIndex}) => Reveal.slide(mainDispatcherIndex + insertPointSlideIndex))
                  .then(() => answerWithCorrection({ ...message, action: 'add' }));
            }
            this.setByMessage = true;
         });
      }

      /**
       * 
       * add hiras with class HiraManager.CLASS_AUTO_ADD to the querystring
       * @memberof HiraManager
       * @param {number} delay [optional] delay the call by the specified time
       * @param {HashChangeEvent}  event [optional] event given when bound to onHashChange
       */
      updateQueryString(delay: number, event?: HashChangeEvent): void {
         if(delay > 0) {
            setTimeout(this.updateQueryString.bind(this, 0, event), delay);
            return;
         }
         const queryString = new URL(location.href).searchParams;
         const hash = location.hash;
         const listOfHira: string[] = Array.prototype.map.call(document.querySelectorAll('.' + HiraManager.CLASS_AUTO_ADD + ' > hr-hira'), (elt: HrHira) => {
            if(elt.highlights.length > 0)
               return elt.getAttribute('hira-id') + ':' + elt.highlights.join('-');
            else
               return elt.getAttribute('hira-id');
         });

         queryString.set('hiras', listOfHira.join(','));
         queryString.set('pos', window.Reveal.getIndices().h.toString());
         // this.baibolyDispatcher.updateQueryString(0);
         window.history.pushState(null, '', `?${queryString + hash}`);
         // window.history.pushState(null, '', newQueryString.replace(/&{2,}/,'&') + hash);
      }

      



   }
