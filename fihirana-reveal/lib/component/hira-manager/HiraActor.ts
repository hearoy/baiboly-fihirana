export function jsonParse<T>(options: any) {
   const { url,
      cache = 'default',
      headers = {
         'Content-Type': 'application/json',
         'Cache-Control': 'public, max-age=600'
      }
   } = options;
   const request = {//new Request(url, {
      method: 'GET',
      cache,
      headers
   };
   return fetch(url, request).then<T>(rsp => rsp.json());
}

function* processTreeByChunk(tree: HTMLTemplateElement, nbChunk: number) {
   const childrenCount = tree.content.childElementCount;
   for(let i = 0; i < nbChunk || i < childrenCount; i++){
      const child = tree.content.children[i];
      yield child
   } 
}