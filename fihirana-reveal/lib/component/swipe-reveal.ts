import { PolymerElement, html } from "@polymer/polymer";
import { customElement,property, query } from "@polymer/decorators";
import { addEventListenerTool } from 'src/tools';

@customElement('swipe-reveal')
class SwipeReveal extends PolymerElement {

   static get template() {
      return html`
      <style>
          :host {
            opacity: 0;
            height: 0;
            width: 0;
         }

      </style>
      `;
   }

   @property({ type: String })
   target = '.slides'
   
   @property({ type: String })
   thresholdX;

   @property({ type: Number })
   thresholdY: number;
   private _target: HTMLElement;

   action: 'swipe-right' | 'swipe-left' | '';
   xStart: number;
   yStart: number;

   connectedCallback() {
      super.connectedCallback();
      this.thresholdX = (window.innerWidth / 5).toString();
      this.thresholdY = window.innerHeight / 10;
   }
   ready() {
      super.ready();
      const _target = this._target = document.querySelector<HTMLElement>(this.target)
      addEventListenerTool(_target, 'touchstart', this._onTouchStart.bind(this)).untilDisconnected(this);
      addEventListenerTool(_target, 'touchend', this._onTouchEnd.bind(this)).untilDisconnected(this);
      addEventListenerTool(_target, 'touchmove', this._onTouchMove.bind(this)).untilDisconnected(this);
   }

   _onTouchMove(e) {
      const touch = e.changedTouches[0];
      const dx = touch.clientX - this.xStart;
      const dy = touch.clientY - this.yStart;
      const angle = this.getAngle(dx,dy);
      const PI = Math.PI;
      const thresX = this.thresholdX;
      const thresY = this.thresholdY;
      // const alpha = Math.abs(angle+PI/2) % PI/2 ;
      // console.log('dx= ' + dx + ', dy= ' + dy, ', angle=' + angle, ', alpha=' + alpha);
      if(Math.abs(dx) >thresX) {
         if ( Math.abs(angle) < PI/4 ) //(Math.abs(dy) < thresY) {
         // if (dx > thresX)
            this.action = 'swipe-right';
         else if (Math.abs(angle) > PI-PI/4)
            this.action = 'swipe-left';
      } else {
         this.action = '';
      }
   }
   _onTouchStart(e) {
      const list = e.changedTouches;
      this.action = '';
      //const startTime;
      if (list.length == 1) {
         this.xStart = list[0].clientX;
         this.yStart = list[0].clientY;
      }
   }
   _onTouchEnd(e) {
      switch (this.action) {
         case 'swipe-left':
            window.Reveal.next();
            break;
         case 'swipe-right':
            window.Reveal.prev();
            break;
      }
   }
   getAngle(dx, dy) {
      return Math.atan2(dy,dx);
   }
}