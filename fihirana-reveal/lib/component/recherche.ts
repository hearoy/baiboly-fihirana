import '@material/mwc-dialog';
import type { HiraWithHighlightsAndIndex, HiraListChangeEventDetail } from './hira-manager/hira-manager';
import { HiraManager } from './hira-manager/hira-manager';
import { addEventListenerTool } from 'src/tools';
import { LitElement, html, css, property, query, customElement, internalProperty } from 'lit-element';

type SpliceNotification = {
   path: string;
   queueProperty: boolean;
   value: {
      indexSplices: {
         index: number;
         removed: any[];
         addedCount: number;
         type: 'splice';
         object: any[];
      }[];
   };
};

@customElement('recherche-hira')
export class RechercheHira extends LitElement {
   @property({ type: Boolean, })
   invalid = true;

   @query('#validerButton')
   validerButton: HTMLButtonElement;
   
   @query('#hiraManager')
   hiraManager: HiraManager;

   @property({ type: Array })
   hiraList: HiraWithHighlightsAndIndex[] = [];

   @property({type: Boolean})
   insertNewHira = true;
   
   /** used from rechercheMotif to preset the input of the dialog */
   @property({type: String})
   hiraValue: string;
   
   @internalProperty()
   openDialog: boolean;

   static get styles() {
      return css`
        :host {
          display: block;
          height: 100%;
          width: 100%;
          --mdc-dialog-max-height: 30rem;
        }
        hira-manager {
          padding: 0;
          margin: 0;
        }
         `;
   }
      

   render() {
      return html`
      <mwc-dialog id="dialog"
                  @opened=${(evt) => this._onDialogOpen(evt)}
                  .open=${this.openDialog}
                  @closed=${this._onDialogClosed}
                  hideActions>
      
        <hira-manager @sections-added=${(evt) => this.close()}
                      .inDialog=${true}
                      id="hiraManager"
                      .insertNewHira=${this.insertNewHira}
                      .hiraList=${this.hiraList}
                      .inputHira=${this.hiraValue}
                      @hira-list-change=${(evt) => this.hiraListChanged(evt)}></hira-manager>
      </mwc-dialog>
      `;
   }

   firstUpdated(changedProperties: Map<string | number | symbol, unknown>) {
      super.firstUpdated(changedProperties);
      const hiraOrchestrator = document.querySelector('#mpitarika-hira') as HiraManager;

      addEventListenerTool(hiraOrchestrator, 'hira-list-change', (evt: CustomEvent<HiraListChangeEventDetail>) => {
         this.hiraList = evt.detail.hiraList;
      })
         .untilDisconnected(this);
      
      addEventListenerTool(this.hiraManager, 'hira-list-change', (evt: CustomEvent) => {
         hiraOrchestrator.hiraList = this.hiraList;
      })
         .untilDisconnected(this);
      // force invalid=true to trigger the event, otherwise the button is displayed the first time
   }
   
   close(){
     this.openDialog = false;
   }

   open() {
      this.openDialog = true;
   }
   _onDialogOpen(e) {
      var alwaysFalse = function () {
         return false;
      };
      window.Reveal.configure({
         keyboardCondition: alwaysFalse
      });
      this.hiraManager.inputElement.select();
   }
   _onDialogClosed(e: CustomEvent) {
      window.Reveal.configure({
         keyboardCondition: null
      })
      this.hiraManager.inputHira = '';
      this.openDialog = false;
      this.dispatchEvent(new CustomEvent('closed', { bubbles: true, composed: true })); // composed so rechercheMotif can pick the event up
   }
   
   private hiraListChanged(evt: CustomEvent) {
      this.hiraList = (evt.currentTarget as HiraManager).hiraList;
   }
}