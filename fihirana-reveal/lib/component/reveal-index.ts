import { observer } from '@material/mwc-base/observer';
import "@material/mwc-icon-button";
import "@material/mwc-ripple";
import 'dark-mode-toggle';
import {DarkModeToggle, ColorScheme} from 'dark-mode-toggle';
import { css, customElement, html, LitElement, property, query } from "lit-element";
import { nothing } from 'lit-html';
import type { BaibolySequence } from "src/baiboly-sequence/baiboly-sequence";
import { clearSlideEventFactory } from 'src/model/slide';
import { addEventListenerTool } from 'src/tools';
import type { HrHira } from "./hr-hira/hr-hira";
import { setTimeoutPromise } from '../../../src/tools';
import { viewCarousel, repeat, clear } from 'src/tools/my-icons';

const HORIZONTAL_SELECTOR = '.slides>section';
/** 
* :scope needed to work around querySelector quirk
* @see https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelectorAll#Quirks
*/
const VERTICAL_FROM_PARENT_SELECTOR = ':scope>section';

interface IndexSlide {
  h: number;
  v: number;
  title: string;
  type: 'hira' | 'baiboly' | '';
  isCurrent: boolean;
}

declare global {
  interface HTMLElementTagNameMap {
    'reveal-index': RevealIndex;
  }
}

let slideChangeListener: ReturnType<typeof addEventListenerTool>;

@customElement('reveal-index')
class RevealIndex extends LitElement {
  @query('.list')
  list: HTMLElement;
  @query('dark-mode-toggle')
  darkModeToggle: DarkModeToggle;

  static styles = css`
          :host {
           display: block;
           overflow:  auto;
           height: 100%;
           box-sizing: border-box;
           background: var(--fpmao-background);
         }
         :host > .list {
           margin-top: 1rem;
           margin-bottom: 1rem;
           line-height: 1.35;
           padding-left: 1rem;
           font-size: 1.6rem;
           color: var(--fpmao-color-1);
           font-weight: 400;
         }
         .list__item {
           position: relative;
           line-height: 1.3;
           list-style: none;
         }
         .list__item:nth-of-type(n+1) {
           margin-top: 0.5ex;
         }
         .list__item--current{
           font-weight: 600;
           filter: drop-shadow(2px 2px 1px);
           /* box-shadow: inset 2px 2px 10px #00000047; */
           padding: 0 1ex;
         }
         .list__item-outer {
           display: flex;
           align-items: center; 
         }
         .list__anchor {
           color: var(--fpmao-color-1);
           text-decoration: none;
           display: inline-block;
           flex: 1 0 auto;
         }
         .buttons {
           list-style: none;
           display: flex;
           flex-flow: row nowrap;
           justify-content: flex-start;
         }
         .button {
           padding: 2px;
           --mdc-icon-button-size: 38px;
           box-sizing: content-box;
           border: solid 2px #26499355;
           border-radius: 9px;
         }
         .button-clear-slide {
           width: 1em;
           height: 1em;
           --mdc-icon-button-size: 1em;
           padding: 0;
           vertical-align: top;
           font-size: 1.2rem;
           line-height: 0;
           color: red;
           opacity: 0.4;
           margin-right: 1rem;
         }
         .button-clear-slide:hover {
           opacity: 1;
         }
         .app-version {
           font-size: 9px;
           color: lightgrey;
           position: absolute;
           top: 0;
           right: 5px;
         }
   
         @media (min-width: 769px) {
           .list__item:nth-of-type(n+1) {
             margin-top: 0;
           }
         }
  `;
  render() {
    function listItemClass(slide: IndexSlide) {
      let cls = 'list__item';
      if (slide.isCurrent)
        cls += ' list__item--current'
      return cls;
    }
    function itemCanBeCleared(item: IndexSlide) {
      return (['baiboly', 'hira'] as (IndexSlide['type'])[]).includes(item.type);
    }
    function slideUrl(slide: IndexSlide): string {
      const qs = location.search.replace(/pos=\d+/, 'pos=' + slide.h).replace(/\?$/, '');
      return './' + qs + '#/' + slide.h + '/' + slide.v;
    }

    return html`
    <ul class="list">
      <li class="buttons">
        <mwc-icon-button class="button" 
                         alt="overview"
                         @click=${this.toggleOverview}>${viewCarousel}</mwc-icon-button>
        <mwc-icon-button class="button"
                         alt="Basculer entre vakiteny et tononkira"
                         @click=${this.switchVakiteny}>${repeat}</mwc-icon-button>
        <mwc-icon-button class="button"
                         alt="overview"
                         @click=${this.switchVakiteny}>${repeat}</mwc-icon-button>
        <dark-mode-toggle @colorschemechange=${e => this.switchColorScheme(e.detail.colorScheme)}></dark-mode-toggle>
      </li>
      ${this.slides.map(item => html`
      <li class=${listItemClass(item)}>
        <div class="list__item-outer">
          ${itemCanBeCleared(item) ? html`
          <mwc-icon-button class="button-clear-slide"
                           alt="clear"
                           @click=${(event) => this.removeSlide(event, item)}>${clear}</mwc-icon-button>
          ` : nothing}
          <a class="list__anchor"
             href=${slideUrl(item)}
             data-pos-h=${item.h}
             data-pos-v=${item.v}>${item.title}</a>
          <mwc-ripple></mwc-ripple>
        </div>
      </li>
      `)}
    
    </ul>
    <aside class="app-version">version: ${window.fpmao_fanipazana.version}</aside>
          `;
  }


  @observer(function _onOpen(this: RevealIndex, newV: boolean, oldV: boolean) {
    if (newV == false) {
      slideChangeListener?.removeNow?.();
      return;
    } else {
      setTimeout(_ => {
        this.updateIndex();
      }, 200);
      slideChangeListener = addEventListenerTool(Reveal as any, 'slidechanged', () => this.updateIndex());
    }
  })
  @property({ type: Boolean })
  displayed = false;

  @property({ type: String })
  selector = 'section';

  @property({ type: Array })
  slides: IndexSlide[] = [];

  connectedCallback() {
    super.connectedCallback();
  }
  
  firstUpdated(changedProperties: Map<string | number | symbol, unknown>) {
    super.firstUpdated(changedProperties);
    this._addClickOnListListener();
    this.switchColorScheme(this.darkModeToggle.mode);
  }

  /**
   * Add `click` on list listener to go to the slide when clicking on its name/title
   * The targets are embedded in the anchor's dataset.posH and dataset.posV 
   */
  _addClickOnListListener() {
    addEventListenerTool(this.list, 'click', evt => {
      if (evt.target != evt.currentTarget && (<HTMLElement>evt.target).matches('.list__anchor')) {
        evt.preventDefault();
        const anchor = <HTMLElement>evt.target;
        window.Reveal.slide(anchor.dataset.posH, anchor.dataset.posV);
        this.displayed = false;
        this.dispatchEvent(new CustomEvent('slide-clicked'));
      }
    })
      .untilDisconnected(this);
  }
  

  private updateIndex() {
    const hSlides = document.querySelectorAll(HORIZONTAL_SELECTOR);
    const slides: IndexSlide[] = [];
    let hiraIndex = -1;
    for (let idx = 0; idx < hSlides.length; idx++) {
      const s = hSlides[idx];
      const vSlides = s.querySelectorAll(VERTICAL_FROM_PARENT_SELECTOR);
      let title: string;
      let hira = <HrHira>s.querySelector('hr-hira');
      let type: IndexSlide['type'];
      /* if the slide holds a hr-hira, take its title */
      if (hira) {
        let prefix = '';
        if (!hira.title.includes('Fanekem-pinoana') && hira.hiraId != 'an_1') {
          hiraIndex++;
          prefix = String.fromCharCode('a'.charCodeAt(0) + hiraIndex) + ') ';
        }
        title = prefix + hira.title;
        const andininy = hira.highlights.length ? ': ' + hira.highlights.join('-') : '';
        title += andininy;
        type = 'hira';
      } /* else, just take the index */
      else {
        title = s.id ? s.id : `slide ${idx}`;
        type = '';
      }
      /* add the horizontal slide to the array */
      slides.push({
        h: idx,
        v: 0,
        title,
        isCurrent: s.matches('.present'),
        type,
      });
      /* process vertical slides, children of the current slide */
      if (vSlides.length > 0) {
        for (let vIdx = 0; vIdx < vSlides.length; vIdx++) {
          const sV = vSlides[vIdx];
          const hira = <HrHira>sV.querySelector('hr-hira');
          const bSeq = <BaibolySequence>sV.querySelector('baiboly-sequence');
          if (hira) {
            title = hira.title;
            type = 'hira';
          }
          else if (bSeq) {
            title = bSeq.books[0];
            type = 'baiboly';
          }
          else {
            title = sV.id ? sV.id : `slide ${idx},${vIdx}`;
            type = '';
          }
          /* add the vertical slide to the array */
          slides.push({
            h: idx,
            v: vIdx,
            title,
            isCurrent: sV.matches('.present'),
            type,
          });
        }
      }
    }
    this.slides = slides;
  }

  removeSlide(event: MouseEvent, item: IndexSlide) {
    if (item.type !== '') {
      this.dispatchEvent(clearSlideEventFactory({
        coords: { h: item.h, v: item.v },
        type: item.type,
      }));
      setTimeoutPromise().then(() => { // wait for rendering
        if (item.isCurrent) {
          Reveal.prev(); // triggers a `slidechanged` event that triggers an `updateIndex`
        } else {
          this.updateIndex();
        }
      })
    }
  }

  private switchColorScheme(colorScheme: ColorScheme) {
    const docHead = document.head;
    const themes = docHead.querySelectorAll<HTMLStyleElement>('style[media*=prefers-color-scheme], style[data-theme-light], style[data-theme-dark]');
    
    for (const theme of themes) {
      if (theme.media.includes('dark') || theme.dataset.themeDark) {
        theme.dataset.themeDark = 'true';
          toggleTheme(theme, colorScheme === 'dark' ? 'on' : 'off');
      } else {
        theme.dataset.themeLight = 'true';
        toggleTheme(theme, colorScheme === 'dark' ? 'off' : 'on');
      }
    }

    function toggleTheme(theme: HTMLStyleElement, mode: 'on' | 'off') {
      theme.media = mode === 'on' ? 'all' : 'not all';
      // @ts-expect-error
      theme.disabled = mode === 'on' ? false : true;
    }
  }

  switchVakiteny() {
    this.displayed = false;
    setTimeout(() => {
      MyKeybindings.toggleVakiteny()
    }, 100);
  }

  toggleOverview() {
    if (window.Reveal) {
      window.Reveal.toggleOverview();
      this.dispatchEvent(new CustomEvent('overview-clicked'));
    }
  }
}