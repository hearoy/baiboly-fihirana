import { observer } from '@material/mwc-base/observer';
import '@material/mwc-button';
import '@material/mwc-icon-button';
import '@material/mwc-menu';
import 'baiboly/res/icons/myicons';
import {
   css,
   customElement,
   html,
   internalProperty,
   LitElement,
   property,
   PropertyValues,
   query
} from 'lit-element';
import { ClassInfo, classMap } from 'lit-html/directives/class-map';
import { clearSlideEventFactory, SlideClearee } from 'src/model/slide';
import { ScrollableKo, Scroller_ko } from 'src/scroller-ko/scroller-ko';
import { addEventListenerTool } from 'src/tools';
import { iconFormatSize, iconZoomIn, iconZoomOut } from 'src/tools/my-icons';

export namespace Fihirana {
   export interface AndininyJson {
      num: string;
      lyrics: string;
      type: 'verse' | 'refrain';
   }

   export interface HiraJson {
      num: string;
      type: 'ffpm' | 'ff' | 'antema' | 'fp';
      title: string;
      version: string[];
      lyrics: AndininyJson[];
      id: string;
   }
   export type FihiranaJson = {[id: string]: HiraJson};
}

/**
 * `hr-hira`
 * Retrieves and displays an hira
 *
 * @customElement
 */
@customElement('hr-hira')
export class HrHira extends LitElement implements ScrollableKo, SlideClearee {
   @internalProperty()
   _eltToScroll: HTMLElement;

   @property({
      type: Number,
   })
   index: number;

   @observer(function (this: HrHira, newV, oldV) {
      document.body.style.setProperty(
         '--lyrics-verse__fontSize',
         `${newV}px`
      );
   })
   @internalProperty()
   _fontSizeLyrics: number = Number((document.body.style.getPropertyValue('--lyrics-verse__fontSize') ?? '30px').replace('px', ''));

   @observer(function (this: HrHira, n, o) {
      this._updateFontSizeTitle(n, o);
   })
   @internalProperty()
   _fontSizeTitle: number = 40;

   @internalProperty()
   _highlights: boolean[] = ((_) => [])();

   @property({
      type: Array,
   })
   highlights: number[] = [];

   @observer(function (this: HrHira, n, o) {
      this._updateHiraNum(n, o);
   })
   @property({
      type: Object,
   })
   hira: Fihirana.HiraJson = {} as Fihirana.HiraJson;

   set hiraId(id: string) {
      this.setAttribute('hira-id', id);
   }
   get hiraId(): string {
      return this.getAttribute('hira-id');
   }

   @internalProperty()
   private menuOpened: boolean;

   mql: MediaQueryList;

   @query('.lyrics__container')
   lyricsContainerElt: HTMLElement;

   @query('#menu-button')
   private fontSizeMenuButton?: HTMLElement;

   @query('scroller-ko')
   scroller: Scroller_ko;

   title: string;

   static get styles() {
      return css`
         :host {
            display: flex;
            flex-flow: column nowrap;
            flex: 1;
            overflow: hidden;
            height: 100vh;
         }
         .header {
            margin: 0;
            line-height: 1;
            flex: 0 0 auto;
            display: flex;
            padding: 0 0.75rem;
            flex-flow: row wrap;
            position: relative;
            justify-content: flex-end;
            align-items: center;
         }
         :host(.scrolled) .header::after {
            opacity: 1;
         }
         .header::after {
            content: '';
            display: block;
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 10px;
            box-shadow: 0px 6px 6px rgba(0, 0, 0, 0.4);
            opacity: 0;
            transition-delay: 0s;
            transition-duration: 50ms;
            transition-property: opacity;
            transition-timing-function: ease-in;
         }

         .title {
            flex: 1 0 100%;
            color: var(--title-color);
            font-size: var(--title-font-size);
            font-weight: var(--title-font-weight);
            font-family: var(--title-font-family);
            line-height: 1;
            margin: 1rem;
         }

         @media screen and (max-width: 425px) {
            .title {
               margin: 0.6rem;
            }
         }

         .buttons {
            flex: 0 0 auto;
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 0.25rem;
         }

         .buttons--absolute {
            position: absolute;
         }

         .buttons--relative {
            position: relative;
         }
         /* .font-sizer::before {
     content: '';
     display: block;
     width: 100%;
     height: 100%;
     padding-top: 50%;
   } */

         .font-sizer__container {
            padding: 1rem;
         }
         .font-sizer {
            flex: 0;
            background-image: linear-gradient(
               0deg,
               rgba(255, 255, 255, 0.1),
               rgba(150, 150, 150, 0.2)
            );
            border: solid 1px rgba(50, 50, 50, 0.1);
            box-shadow: 1px 1px 3px -2px rgba(100, 100, 100, 0.2);
            background-color: rgb(0, 77, 255);
            margin: 0 0.2rem;
            color: white;
            --mdc-icon-size: 2rem;
         }
         :host .lyrics__container {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
            align-items: baseline;
            align-content: stretch;
            /*alignement of flex lines*/
            flex: 1 0 0%;
            padding: 0 1ex;
            overflow-y: auto;
            position: relative;
            padding-bottom: 110px;
         }

         /* :host .lyrics__verse-container:last-of-type {
     margin-bottom: 110px;
   } */

         :host .lyrics__verse-container,
         :host p.andininy,
         :host p.fiverenan-kira {
            flex: 0 1 auto;
            text-align: start;
            padding: 0.6em;
            margin: 0 5px 10px 0;
            border: 2px solid transparent;
            font-family: var(--lyrics-verse__fontFamily);
         }

         :host .lyrics__verse-container--selected,
         :host p.andininy--selected,
         :host p.fiverenan-kira--selected {
            /* padding: 1ex calc( 1.25em - 1ex); */
            border-color: rgb(72, 0, 255);
            /* margin: 0 5px 10px 0; */
            box-shadow: grey 4px 7px 10px;
         }

         .lyrics__verse-number {
            /* float: left; */
            font-size: calc(var(--lyrics-verse__fontSize) - 0.3rem);
            font-style: italic;
            margin-right: 1ex;
         }

         :host .lyrics__verse,
         :host .lyrics__verse--fp {
            margin: 0;
            white-space: pre-line;
            max-width: 30em;
            font-size: var(--lyrics-verse__fontSize);
         }
         :host .lyrics__verse--fp {
            line-height: 1.4;
         }
         :host .lyrics__verse--fp > .lyrics__verse-number {
            display: none;
         }

         :host p.fiverenan-kira {
            order: 2;
         }

         :host span.fiverenan-kira {
            margin-right: 0;
         }
         #menu-button {
            margin-left: 5px;
         }
         scroller-ko {
            margin: 3.5rem 0;
         }
      `;
   }

   render() {
      return html`
         <scroller-ko
            .elt=${this._eltToScroll}
            font-size=${this._fontSizeLyrics}></scroller-ko>
         <div class="header">
            <h3 class="title">${this.title}</h3>
            <div class="buttons buttons--absolute">
               <mwc-icon-button id="menu-button"
                                @click=${() => (this.menuOpened = !this.menuOpened)}>${iconFormatSize}</mwc-icon-button>
               <mwc-menu .anchor=${this.fontSizeMenuButton}
                         .corner=${'BOTTOM_END'}
                         .menuCorner=${'END'}
                         .open=${this.menuOpened}
                         @closed=${(e) => {
                            this.menuOpened = false;
                         }}>
                  <div class="buttons font-sizer__container">
                     <mwc-icon-button
                        class="font-sizer font-sizer--decrease"
                        @click=${this.decreaseFontSize}>${iconZoomOut}</mwc-icon-button>
                     <mwc-icon-button
                        class="font-sizer font-sizer--increase"
                        @click=${this.increaseFontSize}>${iconZoomIn}</mwc-icon-button>
                  </div>
               </mwc-menu>
            </div>
         </div>

         <div class="lyrics__container">
            ${this.hira.lyrics.map(
               (item, index) => html`
                  <div class=${classMap(_andininyClass.call(this, index, item))}
                       @click=${(e) => this._toggleSelected(e, item, index)}>
                     <p class=${classMap(_paragraphClass.call(this))}><span class="andininy lyrics__verse-number">${_numOrRefrain.call(this, item)}</span>${item.lyrics}</p>
                  </div>
               `
            )}
         </div>
      `;

      function _andininyClass(
         this: HrHira,
         idx: number,
         verse: Fihirana.AndininyJson
      ): ClassInfo {
         return {
            andininy: true,
            'lyrics__verse-container': true,
            'lyrics__verse-container--selected': this._highlights[idx],
            'andininy--refrain': verse.type === 'refrain',
         };
      }

      function _paragraphClass(this: HrHira): ClassInfo {
         return {
            'lyrics__verse--fp': this.hira.type === 'fp',
            lyrics__verse: this.hira.type !== 'fp',
         };
      }

      function _numOrRefrain(this: HrHira, item: Fihirana.AndininyJson) {
         const type = item.type;
         if (type === 'refrain') {
            return "Isan'andininy :";
         } else if (this.hira.type == 'fp') {
            return '';
         } else {
            return item.num;
         }
      }
   }

   constructor() {
      super();
      this.mql = window.matchMedia('screen and (max-width: 425px)');
      this.mql.addListener(this._onMediaQueryChange.bind(this));
   }

   clearMe(opts: {coords: {h: number; v: number}}): void {
      const {coords} = opts;
      this.dispatchEvent(
         clearSlideEventFactory({
            coords,
            type: 'hira',
         })
      );
   }

   connectedCallback() {
      super.connectedCallback();

      this._fontSizeLyrics = 30;
      this._onMediaQueryChange(this.mql);
      if (this._highlights.length == 0)
         this._highlights = this.hira.lyrics.map((val) => false);

      this._highlights = this.hira.lyrics.map((val) => {
         if (val.type == 'verse' && this.highlights.includes(Number(val.num)))
            return true;
         else return false;
      });
   }

   firstUpdated(changes: PropertyValues) {
      super.firstUpdated(changes);

      this._eltToScroll = this.lyricsContainerElt;
      addEventListenerTool(
         this.lyricsContainerElt,
         'scroll',
         this._onScroll.bind(this),
         {passive: true, capture: false}
      ).untilDisconnected(this);
   }

   notifyHighlightsChanged() {
      const event = new CustomEvent('highlights-changed', {
         bubbles: true,
         composed: true,
         detail: {},
      });
      this.dispatchEvent(event);
   }

   _onMediaQueryChange(evt: MediaQueryList) {
      if (evt.matches) {
         /* screen and (max-width: 425px) */
         this._fontSizeLyrics = 18;
         this._fontSizeTitle = 22;
      } else {
         this._fontSizeLyrics = 30;
         this._fontSizeTitle = 40;
      }
   }

   _onScroll(e: MouseEvent) {
      if (e.currentTarget == e.target) {
         if (this.lyricsContainerElt.scrollTop > 0)
            this.classList.add('scrolled');
         else this.classList.remove('scrolled');
      }
   }

   _toHighlightsIndices(newV: boolean[]) {
      const res: number[] = [];
      for (let i = 0; i < newV.length; i++) {
         const val = newV[i];
         if (val && this.hira.lyrics[i].type == 'verse')
            res.push(Number(this.hira.lyrics[i].num));
      }
      return res;
   }

   _toggleSelected(e: MouseEvent, item: Fihirana.AndininyJson, index: number) {
      const elt = e.currentTarget;
      if (this.menuOpened) {
         return;
      }
      const prev = this._highlights[index];
      this._highlights[index] = !prev;
      this.highlights = this._toHighlightsIndices(this._highlights);
      this._highlights = this._highlights.slice();
      //  this.notifySplices('_highlights', [{ index, removed: [prev], addedCount: 1, object: this.highlights, type: 'splice' }]);
      this.updateQueryString();
   }

   private updateQueryString() {
      const queryString = new URL(location.href).searchParams;
      const hiras = decodeURIComponent(queryString.get('hiras')).split(',');
      const currentHiraRepresentation = hiras[this.index].replace(
         /([^:]*)(:(.*))?/,
         (
            match: string,
            hira: string,
            _: string | undefined,
            highlights: string | undefined
         ) =>
            hira +
            (this.highlights.length > 0 ? `:${this.highlights.join('-')}` : '')
      );
      hiras.splice(this.index, 1, currentHiraRepresentation);
      queryString.set('hiras', hiras.toString());
      window.history.pushState(
         null,
         window.document.title,
         `?${queryString + window.location.hash}`
      );
   }

   _updateFontSizeTitle(newV, oldV) {
      this.style.setProperty('--title-font-size', `${newV}px`);
   }

   _updateHiraNum(newHira, oldHira) {
      this.title = this.getTitle(newHira);
      const type = newHira.type;
      const num = newHira.num;
      let typeId;
      switch (type) {
         case 'ffpm':
            typeId = 'ffpm';
            break;
         case 'antema':
            typeId = 'an';
            break;
         case 'ff':
            typeId = 'ff';
            break;
         case 'fp':
            typeId = 'fp';
            break;
         default:
            typeId = '';
      }
      this.hiraId = typeId + '_' + num;
   }

   decreaseFontSize() {
      this._fontSizeLyrics -= 2;
   }

   getTitle(hira: Fihirana.HiraJson) {
      if (hira) {
         const type = hira.type;
         const num = hira.num;
         let title: string;
         let version = '';
         if (/_v\d+/.test(hira.id)) {
            /** this is a multi version hira
             * but this is not the first version
             */
            const matches = hira.id.match(/_v(\d+)/);
            if (matches && matches.length > 1)
               version = ' (' + matches[1] + ')';
         } else if (hira.version.length > 0) {
            /** this is a multi version hira
             * and this is the first version
             */
            version = ' (1)';
         }
         switch (type) {
            case 'ffpm':
               title = '';
               break;
            case 'antema':
               title = 'Antema ';
               break;
            case 'ff':
               title = 'FF ';
               break;
            case 'fp':
               return (title = hira.title);
            default:
               title = '';
         }
         return title + num + version;
      }
   }

   increaseFontSize() {
      this._fontSizeLyrics += 2;
   }

   scrollDown() {
      this.scroller.scrollDown();
   }
   scrollUp() {
      this.scroller.scrollUp();
   }
}
