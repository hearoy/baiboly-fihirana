export namespace Fihirana {
    export interface AndininyJson {
      num: string,
      lyrics: string,
      type: 'verse' | 'refrain'
    }
  
    export interface HiraJson {
      num: string,
      type: 'ffpm' | 'ff' | 'antema',
      title: string,
      version: string[],
      lyrics: AndininyJson[]
    }
    export type FihiranaJson = { [id:string]:HiraJson }[]
}
 
 class Uploader{
    uploadedPojo: any;
 
    loadFromInput(input: HTMLInputElement){
       return new Promise((res, rej) => {
          input.addEventListener('change', evt => res(evt));
          input.click();
       }).then(evt => input.files);
    }
     readFileAsText(files: FileList|null) {
         if(files === null) {
             return Promise.resolve(null);
        }
       return new Promise<string>((res, rej) => {
          const fr = new FileReader();
          fr.addEventListener('load', (resString)=>res(fr.result as string));
          fr.readAsText(files[0]);
       });
    }
    uploadFile() {
       const input: HTMLInputElement = document.createElement('input');
       input.type = 'file';
       input.style.display = "none";
       document.body.appendChild(input);
       this.loadFromInput(input)
          .then(this.readFileAsText)
           .then(txt => {
               if(txt === null) {
                   return new Error();
               } else {
                   return JSON.parse(txt);
               }
           })
          .then(pojo => this.uploadedPojo = pojo);
    }
 }

class Converter{
    converted: any = {};
    convert(obj:Fihirana.FihiranaJson) {
        for(let id of Object.keys(obj)) {
            //@ts-ignore
            const hira = obj[id] as Fihirana.HiraJson;
           this.converted[id] = this.includeId(hira, id);
       }
    }
    includeId(hira: Fihirana.HiraJson, id: string) {
       return Object.assign({}, hira, { id });
    }
     
 }

var HiraStorer = class HiraStorer{
   json: Object;
   _hiras: HTMLElement[];

   constructor(){
       this.json = {};
       this.store();
   }
    static main() {
        var hs = new HiraStorer();
        hs.store();
        console.dirxml(hs.json);
   } 
   get hiras(){
       return this._hiras ? this._hiras : document.querySelectorAll('section.hira');
   }
   store(){
       const formatAndininy = (it,typeA,num)=>{
           // process 1 andininy
           let lyrics = '';
           for(const n of it.childNodes){
               if(!n.tagName && n instanceof Text)
                   lyrics += n.data;
                else if(n.tagName && n.matches('.andininy'))
                   num = n.innerText.trim().replace(/\n+/,'\n');
           }
           return {
               num: num,
               lyrics: lyrics.trim(),
               type: typeA
           }
       };

       for( let h of this.hiras.values()){
           const hMatch = h.id.match(/(ff.*|an.*)_(\d+)(\((\d)\))?/i); // e.g. ffpm_88(2)
           let id = hMatch[1] + '_' + hMatch[2];
           const num = hMatch[2];
           const type = hMatch[1].toLowerCase();
           const version = hMatch[3]? hMatch[4] : 0;
           if(hMatch[3]){
              if (Number(version) != 1) {
                 const newId = id + '_v' + version;
                 this.json[id.toLowerCase()].version.push(newId);
                 id = newId;
              }
           }
           const titleHeading = h.querySelector('h3');
           const lyricsContainer = titleHeading.nextElementSibling;
           let andininys=[];
           let typeA;
           const lyCh = lyricsContainer.children;

           
           // process 1 hira
           let numA = 0; // counter for the index of the verses
           for(let i=0; i<lyCh.length;i++){
               const el = lyCh[i];
               if(el.matches('p.andininy')){
                   typeA= 'verse';
                   numA++;
               }else if(el.matches('p.fiverenan-kira')){
                   typeA='refrain';
               } else{
                   typeA='other';
               }
               const elFormatted = formatAndininy(el,typeA,numA);
               andininys.push(elFormatted);
           }
           
           this.json[id.toLowerCase()] = {
               num : num,
               type: type,
               title: titleHeading.textContent.replace(/\d+ *(\(\d\))* */,'').trim(),
               version: [],
               lyrics: andininys
           };
       }    
   }
   
}



//copy(hs.json);