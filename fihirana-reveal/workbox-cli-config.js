module.exports = {
  "cacheId": "fihirana-fpmao",
  "globDirectory": "build/public/",
  "globIgnores": [
    "../../workbox-cli-config.js"
  ],
  "globPatterns": [
    "bower_components/webcomponentsjs/webcomponents*.js",
    "js/reveal.min.js",
    "app-shell.html",
    "lib/font/**/*.woff2",
    "lib/font/**/*.css",
    "fihirana.html",
    "fihirana.json",
    "index.html",
    "res/enTeteAfficheFPMAOCentre.jpg",
    "res/enTeteAfficheFPMAOCentreRoundSketchTraits*.jpg",
    "**/*.html"
  ],
  "handleFetch": true,
  "ignoreUrlParametersMatching": [/./],
  "runtimeCaching": [
    {
      "urlPattern": /\.(css|js|woff2|jpg)$/,
      "handler": "cacheFirst",
      "options": {
        "cacheName": "runtimeCache"
      }
    },
    {
      "urlPattern": /https:\/\/fonts.googleapis.com\/css.*/,
      "handler": "cacheFirst",
      "options": {
        "cacheName": "runtimeCache"
      }
    }
  ],
  "swDest": "build/public/sw.js",
  "skipWaiting": true
};
